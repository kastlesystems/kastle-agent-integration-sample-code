﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Service.Helpers
{
    public static class SystemLogger
    {
        /// <summary>
        /// The default priority
        /// </summary>
        private const Priority DefaultPriority = Priority.None;
        /// <summary>
        /// The default severity
        /// </summary>
        private const TraceEventType DefaultSeverity = TraceEventType.Information;
        /// <summary>
        /// The default event identifier
        /// </summary>
        private const int DefaultEventId = 1;
        /// <summary>
        /// The default title
        /// </summary>
        private const string DefaultTitle = "";
        /// <summary>
        /// The default category
        /// </summary>
        private static readonly List<string> DefaultCategory = new List<string> { "myKASTLE" };
        /// <summary>
        /// 
        /// </summary>

        #region Title Constants
        private const string ErrorTitle = "Error";
        /// <summary>
        /// The information title
        /// </summary>
        private const string InfoTitle = "Information";
        /// <summary>
        /// The warn title
        /// </summary>
        private const string WarnTitle = "Warning";
        /// <summary>
        /// The debug title
        /// </summary>
        private const string DebugTitle = "Debug";
        #endregion

        #region Error Log Methods        
        /// <summary>
        /// Writes the error.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void WriteError(Exception ex)
        {
            var log = new LogEntry
            {
                Message = ex.ToString(),
                TimeStamp = DateTime.Now,
                Categories = DefaultCategory,
                Priority = (int)Priority.High,
                EventId = DefaultEventId,
                Severity = TraceEventType.Error,
                Title = ErrorTitle
            };
            Logger.Write(log);
        }

        /// <summary>
        /// This method will log error
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        public static void WriteError(string message)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = DefaultCategory;
            log.Priority = (int)Priority.High;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Error;
            log.Title = ErrorTitle;
            Logger.Write(log);
        }

        /// <summary>
        /// This method will log error
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        public static void WriteError(string message, string[] categories)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.High;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Error;
            log.Title = ErrorTitle;
            Logger.Write(log);
        }

        /// <summary>
        /// This method will log error
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        /// <param name="properties" Type=IDictionary<string, object>This will contain additional information that you want to log</param>
        public static void WriteError(string message, string[] categories, IDictionary<string, object> properties)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.High;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Error;
            log.Title = ErrorTitle;
            log.ExtendedProperties = properties;
            Logger.Write(log);
        }

        /// <summary>
        /// This method will log error
        /// </summary>
        /// <param name="log">Log contain all the information related to the message to be logged</param>
        public static void WriteError(ErrorMessage errorMessage)
        {
            string message = string.Format("{1}{0}EventId:{2}{0}ErrCode:{3}({4}){0}Stack:{5}{0}",
                        Environment.NewLine,
                        errorMessage.Ex.Message,
                        errorMessage.EventId,
                        errorMessage.ErrCode,
                        (int)errorMessage.ErrCode,
                        errorMessage.Ex.StackTrace);

            if (errorMessage.Categories == null)
                errorMessage.Categories = DefaultCategory;

            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = errorMessage.Categories;
            log.Priority = (int)Priority.High;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Error;
            log.Title = errorMessage.Title;
            Logger.Write(log);
        }

        #endregion

        #region Information Log Methods

        /// <summary>
        /// This method will log information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        public static void WriteInformation(string message)
        {
            try
            {
                LogEntry log = new LogEntry();
                log.Message = message;
                log.TimeStamp = DateTime.Now;
                log.Categories = DefaultCategory;
                log.Priority = (int)Priority.Low;
                log.EventId = DefaultEventId;
                log.Severity = TraceEventType.Information;
                log.Title = InfoTitle;
                Logger.Write(log);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// This method will log information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        public static void WriteInformation(string message, string[] categories)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.Low;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Information;
            log.Title = InfoTitle;
            Logger.Write(log);

        }

        /// <summary>
        /// This method will log information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        /// <param name="properties" Type=IDictionary<string, object>>This will contain additional information that you want to log</param>        
        public static void WriteInformation(string message, string[] categories, IDictionary<string, object> properties)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.Low;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Information;
            log.Title = InfoTitle;
            log.ExtendedProperties = properties;
            Logger.Write(log);

        }
        #endregion

        #region Warning Log Methods

        /// <summary>
        /// This method will log warnings
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        public static void WriteWarning(string message)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = DefaultCategory;
            log.Priority = (int)Priority.Medium;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Warning;
            log.Title = WarnTitle;
            Logger.Write(log);

        }

        /// <summary>
        /// This method will log warnings
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        public static void WriteWarning(string message, string[] categories)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.Medium;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Warning;
            log.Title = WarnTitle;
            Logger.Write(log);

        }

        /// <summary>
        /// This method will log warnings
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        /// <param name="properties" Type=IDictionary<string, object>>This will contain additional information that you want to log</param>        
        public static void WriteWarning(string message, string[] categories, IDictionary<string, object> properties)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories = categories;
            log.Priority = (int)Priority.Medium;
            log.EventId = DefaultEventId;
            log.Severity = TraceEventType.Warning;
            log.Title = WarnTitle;
            log.ExtendedProperties = properties;
            Logger.Write(log);

        }

        #endregion

        #region Debug Log Methods

        /// <summary>
        /// This method will log debug information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        public static void WriteDebug(string message)
        {
            try
            {
                LogEntry log = new LogEntry();
                log.Message = message;
                log.Categories = DefaultCategory;
                log.Title = DebugTitle;
                log.TimeStamp = DateTime.Now;
                Logger.Write(log);
            }
            catch (Exception ex)
            {
                WriteError("Error occurred in WriteDebug(message), Message:" + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
            }
        }

        /// <summary>
        /// This method will log debug information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        public static void WriteDebug(string message, string[] categories)
        {
            try
            {
                LogEntry log = new LogEntry();
                log.Message = message;
                log.Categories = categories;
                log.Title = DebugTitle;
                log.TimeStamp = DateTime.Now;
                Logger.Write(log);
            }
            catch (Exception ex)
            {
                WriteError("Error occurred in WriteDebug(message, categories), Message:" + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
            }
        }

        /// <summary>
        /// This method will log debug information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        /// <param name="categories" Type=string>Categories that will be associated with this log message</param>
        /// <param name="properties" Type=IDictionary<string, object>>This will contain additional information that you want to log</param>        
        public static void WriteDebug(string message, string[] categories, IDictionary<string, object> properties)
        {
            try
            {
                LogEntry log = new LogEntry();
                log.Message = message;
                log.Categories = categories;
                log.Title = DebugTitle;
                log.ExtendedProperties = properties;
                log.TimeStamp = DateTime.Now;
                Logger.Write(log);
            }
            catch (Exception ex)
            {
                WriteError("Error occurred in WriteDebug(message, categories, properties), Message:" + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
            }
        }

        /// <summary>
        /// This method will log debug information
        /// </summary>
        /// <param name="message" Type=string>Message to be logged</param>
        public static void WriteInfoLog(string message)
        {
            try
            {
                StackFrame frame = new StackFrame(1);
                var method = frame.GetMethod();
                var typeName = (method.DeclaringType != null) ? method.DeclaringType.FullName : string.Empty;
                var methodName = method.Name;
                var methodMessage = string.Format("{0}.{1} - {2}", typeName, methodName, message);

                WriteDebug(methodMessage);
            }
            catch (Exception ex)
            {
                WriteError("Error occurred in WriteInfoDebug(), Message:" + ex.Message + Environment.NewLine + "Stack Trace: " + ex.StackTrace);
            }
        }
        #endregion
    }

    public enum Priority : int
    {
        // The values get mapped to the integer Priority values used by Enterprise Library Logging
        DefaultValue = 0,   // we have to assign zero to some member in a non-flag enum hence defined this member
        None = -1,
        Critical = 25,
        High = 20,
        Medium = 15,
        Low = 10,
        Trace = 5,
        Debug = 4
    }

    public class ErrorMessage
    {
        #region Fields
        /// <summary>
        /// The _title
        /// </summary>
        /// <summary>
        /// The _title
        /// </summary>
        private string _title;
        /// <summary>
        /// The _message
        /// </summary>
        private string _message;
        /// <summary>
        /// The _event identifier
        /// </summary>
        private string _eventId;
        /// <summary>
        /// The _do not log
        /// </summary>
        private bool _doNotLog;
        /// <summary>
        /// The _ex
        /// </summary>
        private Exception _ex;
        /// <summary>
        /// The _err code
        /// </summary>
        private ErrorCode _errCode;
        /// <summary>
        /// The _disp code
        /// </summary>
        private string _dispCode;
        /// <summary>
        /// The _categories
        /// </summary>
        private List<string> _categories;
        #endregion Fields

        #region Properties

        private static Dictionary<string, ErrorMessage> _serverErrors = new Dictionary<string, ErrorMessage>();

        /// <summary>
        /// Gets the server errors.
        /// </summary>
        /// <value>The server errors.</value>
        public static Dictionary<string, ErrorMessage> ServerErrors
        {
            get { return _serverErrors; }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        /// <summary>
        /// Gets or sets the event identifier.
        /// </summary>
        /// <value>The event identifier.</value>
        /// <summary>
        /// Gets or sets the event identifier.
        /// </summary>
        /// <value>The event identifier.</value>
        public string EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }
        /// <summary>
        /// Gets or sets a value indicating whether [do not log].
        /// </summary>
        /// <value><c>true</c> if [do not log]; otherwise, <c>false</c>.</value>
        /// <summary>
        /// Gets or sets a value indicating whether [do not log].
        /// </summary>
        /// <value><c>true</c> if [do not log]; otherwise, <c>false</c>.</value>
        public bool DoNotLog
        {
            get { return _doNotLog; }
            set { _doNotLog = value; }
        }
        /// <summary>
        /// Gets or sets the display code.
        /// </summary>
        /// <value>The display code.</value>
        /// <summary>
        /// Gets or sets the display code.
        /// </summary>
        /// <value>The display code.</value>
        public string DisplayCode
        {
            get { return _dispCode; }
            set { _dispCode = value; }
        }
        /// <summary>
        /// Gets or sets the ex.
        /// </summary>
        /// <value>The ex.</value>
        /// <summary>
        /// Gets or sets the ex.
        /// </summary>
        /// <value>The ex.</value>
        public Exception Ex
        {
            get { return _ex; }
            set { _ex = value; }
        }
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        public ErrorCode ErrCode
        {
            get { return _errCode; }
            set
            {
                _errCode = value;
                this.SetExceptionMessage();
            }
        }
        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>The categories.</value>
        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>The categories.</value>
        public List<string> Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }
        #endregion Properties

        #region Constants

        /// <summary>
        /// The database ex MSG
        /// </summary>
        private const string dbExMsg = "DBExceptionMsg";
        /// <summary>
        /// The database sp timeout ex MSG
        /// </summary>
        private const string dbSPTimeoutExMsg = "DBSPTimeoutExMsg";
        /// <summary>
        /// My kastle service ex MSG
        /// </summary>
        private const string myKastleServiceExMsg = "MyKastleServiceExMsg";
        /// <summary>
        /// The security access denied ex MSG
        /// </summary>
        private const string securityAccessDeniedExMsg = "SecurityAccessDeniedExMsg";
        /// <summary>
        /// My kastle ws not responding ex MSG
        /// </summary>
        private const string myKastleWSNotRespondingExMsg = "MyKastleWSNotRespondingExMsg";
        /// <summary>
        /// My kastle ws session timeout ex MSG
        /// </summary>
        private const string myKastleWSSessionTimeoutExMsg = "MyKastleWSSessionTimeoutExMsg";
        /// <summary>
        /// The purchase supplies ex MSG
        /// </summary>
        private const string purchaseSuppliesExMsg = "PurchaseSuppliesExMsg";
        /// <summary>
        /// The service path unknown ex MSG
        /// </summary>
        private const string servicePathUnknownExMsg = "ServicePathUnknownExMsg";
        /// <summary>
        /// The network access issue ex MSG
        /// </summary>
        private const string networkAccessIssueExMsg = "NetworkAccessIssueExMsg";
        /// <summary>
        /// The bad request ex MSG
        /// </summary>
        private const string badRequestExMsg = "BadRequestExMsg";
        /// <summary>
        /// The unauthorized ex MSG
        /// </summary>
        private const string unauthorizedExMsg = "UnauthorizedExMsg";
        /// <summary>
        /// The forbidden ex MSG
        /// </summary>
        private const string forbiddenExMsg = "ForbiddenExMsg";
        /// <summary>
        /// The file not found ex MSG
        /// </summary>
        private const string fileNotFoundExMsg = "FileNotFoundExMsg";
        /// <summary>
        /// The request timeout ex MSG
        /// </summary>
        private const string requestTimeoutExMsg = "RequestTimeoutExMsg";

        #endregion Constants

        #region Methods
        /// <summary>
        /// This method is used to set the title and message of the exception based on the error code.
        /// </summary>
        private void SetExceptionMessage()
        {
            ResourceManager KastleMessageResourceManager = new ResourceManager
            ("Kastle.Common.MyKastleExcMsgs", Assembly.GetExecutingAssembly());

            switch (_errCode)
            {
                case ErrorCode.DBConnectionFailed:
                    //Exception occurred due to incorrect server name, db name, username or password.
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(dbExMsg);
                    _dispCode = ((int)ErrorCode.DBConnectionFailed).ToString();
                    break;
                case ErrorCode.DBBusinessLogicFailed:
                    //Exception occurred due to incorrect business logic.
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(dbExMsg);
                    _dispCode = ((int)ErrorCode.DBBusinessLogicFailed).ToString();
                    break;
                case ErrorCode.DBOperationFailed:
                    //We are setting this error code when any db operation fails and it is not caught in the cases.
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(dbExMsg);
                    _dispCode = ((int)ErrorCode.DBOperationFailed).ToString();
                    break;
                case ErrorCode.DBSPTimeout:
                    //Exception occurred due to SP timeout.
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(dbSPTimeoutExMsg);
                    _dispCode = ((int)ErrorCode.DBSPTimeout).ToString();
                    break;
                case ErrorCode.MyKastleServiceDown:
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(myKastleServiceExMsg);
                    _dispCode = ((int)ErrorCode.MyKastleServiceDown).ToString();
                    break;
                case ErrorCode.MyKastleServiceException:
                    _title = "Service Error";
                    _message = KastleMessageResourceManager.GetString(myKastleServiceExMsg);
                    _dispCode = ((int)ErrorCode.MyKastleServiceException).ToString();
                    break;
                case ErrorCode.SecurityAccessDenied:
                    //This exception can arise when access is denied for a particular operation like read/write file.
                    _title = "Internal Security Exception";
                    _message = KastleMessageResourceManager.GetString(securityAccessDeniedExMsg);
                    _dispCode = ((int)ErrorCode.SecurityAccessDenied).ToString();
                    break;
                case ErrorCode.MyKastleWSNotResponding:
                    //If any exception arises, set this error code. Decide the message.
                    _title = "Error Occurred";
                    _message = KastleMessageResourceManager.GetString(myKastleWSNotRespondingExMsg);
                    _dispCode = ((int)ErrorCode.MyKastleWSNotResponding).ToString();
                    break;
                case ErrorCode.MyKastleWSSessionTimeout:
                    _title = "Session Timeout";
                    _message = KastleMessageResourceManager.GetString(myKastleWSSessionTimeoutExMsg);
                    _dispCode = ((int)ErrorCode.MyKastleWSSessionTimeout).ToString();
                    break;
                case ErrorCode.PurchaseSuppliesException:
                    //This exception is thrown when some error occurs while tax calculation and payment is made in purchase supplies section.
                    _title = "Error in Shipping and Payment";
                    _message = KastleMessageResourceManager.GetString(purchaseSuppliesExMsg);
                    _dispCode = ((int)ErrorCode.PurchaseSuppliesException).ToString();
                    break;
                case ErrorCode.ServicePathUnknown:
                    //Exception occurred due to incorrect cygwin path, web service path or external site url (for ex: tax).
                    _title = "Unknown Service Path";
                    _message = KastleMessageResourceManager.GetString(servicePathUnknownExMsg);
                    _dispCode = ((int)ErrorCode.ServicePathUnknown).ToString();
                    break;
                case ErrorCode.NetworkAccessIssue:
                    //This exception can arise when msdtc service is disabled on the machine on which the services are deployed.
                    _title = "Network Access Issue";
                    _message = KastleMessageResourceManager.GetString(networkAccessIssueExMsg);
                    _dispCode = ((int)ErrorCode.NetworkAccessIssue).ToString();
                    break;
                //Http Status Codes
                case ErrorCode.BadRequest:
                    _title = "Bad Syntax";
                    _message = KastleMessageResourceManager.GetString(badRequestExMsg);
                    _dispCode = ((int)ErrorCode.BadRequest).ToString();
                    break;
                case ErrorCode.Unauthorized:
                    _title = "Unauthorized";
                    _message = KastleMessageResourceManager.GetString(unauthorizedExMsg);
                    _dispCode = ((int)ErrorCode.Unauthorized).ToString();
                    break;
                case ErrorCode.Forbidden:
                    _title = "Forbidden";
                    _message = KastleMessageResourceManager.GetString(forbiddenExMsg);
                    _dispCode = ((int)ErrorCode.Forbidden).ToString();
                    break;
                case ErrorCode.FileNotFound:
                    _title = "File Not Found";
                    _message = KastleMessageResourceManager.GetString(fileNotFoundExMsg);
                    _dispCode = ((int)ErrorCode.FileNotFound).ToString();
                    break;
                case ErrorCode.RequestTimeout:
                    _title = "myKastle WebSite Down";
                    _message = KastleMessageResourceManager.GetString(requestTimeoutExMsg);
                    _dispCode = ((int)ErrorCode.RequestTimeout).ToString();
                    break;
                case ErrorCode.HTTPWebRequestTimeOut:
                    _title = "HTTP web request timed out";
                    _message = KastleMessageResourceManager.GetString(requestTimeoutExMsg);
                    _dispCode = ((int)ErrorCode.HTTPWebRequestTimeOut).ToString();
                    break;
                case ErrorCode.knetmqRequestTimeOut:
                    _title = "knetmq request timed out";
                    _message = KastleMessageResourceManager.GetString(requestTimeoutExMsg);
                    _dispCode = ((int)ErrorCode.knetmqRequestTimeOut).ToString();
                    break;
            }
        }
        #endregion Methods
    }

    public enum ErrorCode
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,
        /// <summary>
        /// The database connection failed
        /// </summary>
        DBConnectionFailed = 101,
        /// <summary>
        /// The database business logic failed
        /// </summary>
        DBBusinessLogicFailed,
        /// <summary>
        /// The database operation failed
        /// </summary>
        DBOperationFailed,
        /// <summary>
        /// The DBSP exception/
        /// </summary>
        DBSPException,
        /// <summary>
        /// The DBSP timeout/
        /// </summary>
        DBSPTimeout,
        /// <summary>
        /// My kastle service down
        /// </summary>
        MyKastleServiceDown = 201,
        /// <summary>
        /// My kastle service exception
        /// </summary>
        MyKastleServiceException,
        /// <summary>
        /// My kastle ws not responding
        /// </summary>
        MyKastleWSNotResponding,
        /// <summary>
        /// My kastle ws session timeout
        /// </summary>
        MyKastleWSSessionTimeout,
        /// <summary>
        /// The purchase supplies exception
        /// </summary>
        PurchaseSuppliesException,
        /// <summary>
        /// The SMTP failed recipient exception
        /// </summary>
        SmtpFailedRecipientException,
        /// <summary>
        /// The service path unknown
        /// </summary>
        ServicePathUnknown = 301,
        /// <summary>
        /// The security access denied
        /// </summary>
        SecurityAccessDenied = 302,
        /// <summary>
        /// The network access issue
        /// </summary>
        NetworkAccessIssue = 303,
        //Http Status Codes:
        BadRequest = 400,
        /// <summary>
        /// The unauthorized
        /// </summary>
        Unauthorized = 401,
        /// <summary>
        /// The forbidden
        /// </summary>
        Forbidden = 403,
        /// <summary>
        /// The file not found
        /// </summary>
        FileNotFound = 404,
        /// <summary>
        /// The request timeout
        /// </summary>
        RequestTimeout = 408,
        /// <summary>
        /// The HTTP web request time out
        /// </summary>
        HTTPWebRequestTimeOut = 409,
        /// <summary>
        /// The SMTP exception
        /// </summary>
        SmtpException,
        /// <summary>
        /// The knetmq request time out
        /// </summary>
        knetmqRequestTimeOut = 410,
        /// <summary>
        /// The socket exception
        /// </summary>
        SocketException,
        /// <summary>
        /// The aggregate exception
        /// </summary>
        AggregateException
    }
}
