﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace Service
{
    public static class Utilities
    {


        public static HttpResponseMessage GetXmlResponse<T>(T objectInstance, HttpRequestMessage request) where T : class
        {
            var response = request.CreateResponse(HttpStatusCode.OK, objectInstance);
            response.Content.Headers.Remove("Content-Type");
            response.Content.Headers.Add("Content-Type", "application/xml");
            response.Content = new StreamContent(Utilities.ObjectToStream(typeof(T), objectInstance));
            response.Content.Headers.Remove("Content-Type");
            response.Content.Headers.Add("Content-Type", "application/xml");
            return response;
        }

        public static MemoryStream ObjectToStream<T>(Type type, T data)
        {
            var xns = new XmlSerializerNamespaces();
            xns.Add("", "urn:psialliance-org");
            var ms = new MemoryStream();
            var ser = new XmlSerializer(type);
            try
            {
                ser.Serialize(ms, data, xns);
            }
            catch (Exception ex)
            {
            }
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        public static T StreamToObject<T>(Stream stream)
        {
            var ser = new XmlSerializer(typeof(T));
            return (T)ser.Deserialize(stream);
        }

        public static ResponseStatus GetResponseStatus(int id, ActionStatusCode statusCode, string message = "")
        {
            var responseStatus = new ResponseStatus
            {
                version = "1.0",
                requestURL = string.Empty,
                statusCode = Convert.ToInt16(statusCode),
                statusString = message,
                id = id.ToString()
            };
            return responseStatus;
        }

        public static string GetSha256Hash(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            var strBuilder = new StringBuilder();
            foreach (byte x in hash)
            {
                strBuilder.Append(string.Format("{0:x2}", x));
            }
            return strBuilder.ToString();
        }

        public static string SerializeObject<T>(T pObject)
        {
            var memoryStream = new MemoryStream();

            var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            try
            {
                var xs = new XmlSerializer(typeof(T));

                xs.Serialize(xmlTextWriter, pObject);

                var doc = new XmlDocument();
                memoryStream.Position = 0;
                doc.Load(memoryStream);
                memoryStream.Close();
                // strip out default namespaces "xmlns:xsi" and "xmlns:xsd"
                if (doc.DocumentElement != null) doc.DocumentElement.Attributes.RemoveAll();

                var sw = new StringWriter();
                var xw = new XmlTextWriter(sw);
                doc.WriteTo(xw);

                return sw.ToString();
            }
            finally
            {
                memoryStream.Close();
                xmlTextWriter.Close();
            }
        }

        public static T DeserializeObject<T>(string xmlString)
        {
            if (String.IsNullOrWhiteSpace(xmlString))
                return default(T);

            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(memStream);
            }
        }

        public static string AppendBrachesInGuid(string guid)
        {
            return new Guid(guid).ToString("B");
        }
    }
}
