﻿using System;
using System.Net;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using Service.Helpers;

namespace Service
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class BasicAuthenticationHandlerAttribute : ActionFilterAttribute
    {
        #region Private Fields

        private IOperationInvoker _invoker;
        public static string IssuerSignatureFromHeader { get; private set; }

        private const string Realm = "Authentication required for PLAI Service";



        public BasicAuthenticationHandlerAttribute()
        {

        }

        #endregion

        #region ActionFilterAttribit Members

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            string outPutMessage = string.Empty;
            if (Authenticate(filterContext,ref outPutMessage)) return;
            filterContext.Response = new System.Net.Http.HttpResponseMessage();
            filterContext.Response.ReasonPhrase = outPutMessage;
            filterContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", Realm));
            filterContext.Response.StatusCode = HttpStatusCode.Unauthorized;
            SystemLogger.WriteError(outPutMessage);
        }

        #endregion


        private bool Authenticate(HttpActionContext filterContext, ref string outPutMessage)
        {
            string requestUri = filterContext.Request.RequestUri.AbsoluteUri;

            var httpVerb = filterContext.Request.Method.Method;



            if (!(requestUri.IndexOf("deviceOwnership", StringComparison.OrdinalIgnoreCase) >= 0))
            {
                IssuerSignatureFromHeader = GetIssuerSignatureFromHeader(filterContext);
                if (PSIAController.IssuerSignature == null || !PSIAController.IssuerSignature.Equals(IssuerSignatureFromHeader))
                {
                    if (PSIAController.IssuerSignature == null)
                    {
                        
                        outPutMessage = string.Format("Authenticate deviceOwnership failed for null issuer signature");
                    }
                    else
                    {
                        outPutMessage = string.Format("Authenticate deviceOwnership failed for wrong issuer signature. PSIAIssuerSignature: " + PSIAController.IssuerSignature + ". IssuerSignarure from header: " + IssuerSignatureFromHeader);
                    }

                    return false;
                }
            }

            string[] credentials = GetCredentialsFromHeaders(filterContext);

            if (credentials != null && credentials.Length == 2)
            {
                // do auth here
                var username = credentials[0];
                var password = credentials[1];

                string adminUserNameInConfig = ConfigurationSettings.AppSettings["AdminName"];
                string adminPasswordInConfig = ConfigurationSettings.AppSettings["AdminPassword"];

                bool isAdmin = adminUserNameInConfig.Equals(username, StringComparison.Ordinal) &&
                       adminPasswordInConfig.Equals(password, StringComparison.Ordinal);

                if (isAdmin)
                    return true;

                //If not admin credentials, match for guest credentials and allow only GET commands
                string guestUserNameInConfig = ConfigurationSettings.AppSettings["AdminName"];
                string guestPasswordInConfig = ConfigurationSettings.AppSettings["AdminPassword"];

                var method = filterContext.Request.Method.Method;

                bool isValidGuest = guestUserNameInConfig.Equals(username, StringComparison.Ordinal) &&
                       guestPasswordInConfig.Equals(password, StringComparison.Ordinal);

                if (isValidGuest)
                {
                    if (method.Equals("GET", StringComparison.OrdinalIgnoreCase))
                        return true;

                    filterContext.Response.StatusCode = HttpStatusCode.MethodNotAllowed;
                    outPutMessage = string.Format("Authenticate failed for IsValidGuest: MethodNotAllowed.");
                    return false;

                }
                else
                {
                    outPutMessage = string.Format("Authenticate failed for IsValidGuest is false.");
                }
            }

            filterContext.Response = new System.Net.Http.HttpResponseMessage();
            filterContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", Realm));
            filterContext.Response.StatusCode = HttpStatusCode.Unauthorized;
            return false;
        }

        //This method is obselete. Would be removed.
        public static string GetIssuerSignatureFromHeader(HttpActionContext filterContext)
        {
            var values = filterContext.Request.Headers.GetValues(ServiceConstants.IssuerSignatureHeader);
            if (values != null)
            {
                return values.FirstOrDefault();
            }
            else
            {
                SystemLogger.WriteError(string.Format("GetIssuerSignatureFromHeader is null."));
            }

            return null;
        }


        /// <summary>
        /// This is obselete. Would throw exception.
        /// </summary>
        /// <returns></returns>
        public static string[] GetCredentialsFromHeaders(HttpActionContext filterContext)
        {
            IEnumerable<string> values = new List<string>();
            filterContext.Request.Headers.TryGetValues("Authorization", out values);

            if (values != null && values.Count() > 0)
            {

                string credentials = values.FirstOrDefault();
                if (credentials != null)
                    credentials = credentials.Trim();

                if (!String.IsNullOrEmpty(credentials))
                {
                    try
                    {
                        string[] credentialParts = credentials.Split(new[] { ' ' });
                        if (credentialParts.Length == 2 && credentialParts[0].Equals("basic", StringComparison.OrdinalIgnoreCase))
                        {
                            credentials = Encoding.ASCII.GetString(Convert.FromBase64String(credentialParts[1]));
                            credentialParts = credentials.Split(new[] { ':' });
                            if (credentialParts.Length == 2)
                                return credentialParts;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return null;
        }
    }
}