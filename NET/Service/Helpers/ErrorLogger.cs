﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ErrorLogger
    {
        public static void WriteToDefaultLog(string message , EventLogEntryType eventLogEntryType)
        {
            var sSource = "AdpterService";
            var sLog = "Application";
          

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, message);
            EventLog.WriteEntry(sSource, message, eventLogEntryType, 234);
        }
    }
}
