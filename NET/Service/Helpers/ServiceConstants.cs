﻿namespace Service
{
    public static class ServiceConstants
    {
        public const string IssuerSignatureHeader = "IssuerSignature";
        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    }
 
}

