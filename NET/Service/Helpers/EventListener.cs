﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service
{
    public class EventListener
    {
        public static ConcurrentDictionary<string, string> EventsCollection { get; set; }
        private string socketIPAddress;
        private int interval;
        private int tcpPort;
        private string netLinkUrl;
        private string netUserName;
        private string netUserPassword;
        private TcpListener tcpListener;
        public string IPAddress
        {
            get { return socketIPAddress; }
        }

        public int TCPPort
        {
            get { return tcpPort; }
        }

        public TcpListener Listener
        {
            get { return tcpListener; }
        }

        public EventListener(int tcpPort, int interval)
        {

            this.tcpPort = tcpPort;
            this.interval = interval;
            EventsCollection = new ConcurrentDictionary<string, string>();


        }


        public void StartEventsListening(CancellationToken toke)
        {
            System.Net.IPAddress ipAddress = System.Net.IPAddress.Any;
            TcpClient tcpClient = null;
            tcpListener = new TcpListener(ipAddress, tcpPort);
            tcpListener.Start();
            Task.Run(() => {
                while (true)
                {
                    tcpClient = tcpListener.AcceptTcpClient();
                    ThreadPool.QueueUserWorkItem(ThreadProc, tcpClient);
                }
            });
        }

        private void ThreadProc(object obj)
        {

            var client = (TcpClient)obj;
            try
            {
                StartStreamDataReceiving(client);
            }
            finally
            {
                client.Close();
            }
            // Do your work here
        }

        private void StartStreamDataReceiving(TcpClient tcpClient)
        {
            byte[] bytes = new byte[2048];
            if (tcpClient.Connected)
            {
                NetworkStream stream = tcpClient.GetStream();
                var i = stream.Read(bytes, 0, bytes.Length);

                while (i != 0)
                {
                    OnNewEventReceived(bytes);
                    try
                    {
                        i = stream.Read(bytes, 0, bytes.Length);
                    }
                    catch (Exception ex)
                    {
                        break;
                    }
                }
            }
            else
            {
               //do logging
            }
        }



        private void OnNewEventReceived(byte[] bytes)
        {
            var eventData = Encoding.UTF8.GetString(bytes);
            if (!string.IsNullOrEmpty(eventData))
                EventsCollection.AddOrUpdate(Guid.NewGuid().ToString(), eventData, (key, oldValue) => eventData);
        }


    }
}
