﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Service
{
    /// <summary>
    ///Usage example:
    /// TimeOutHelper.ExecuteMethod(() =>
    ///{
    /// byte[] Data = new byte[2048];
    /// int cData=-1;
    /// do
    ///     {
    ///         cData = Service.Read(Data, 0, 2048);
    ///         Thread.Sleep(100);
    ///     } while (cData == 0);
    ///},5000);
    /// </summary>
    public  static class TimeOutHelper
    {
        private class ObjectState
        {
            public Action predict { get; set; }
            public AutoResetEvent autoEvent { get; set; }
        }
        /// <summary>
        /// Tries to execute methods in given time. If given time is exceeded throws TimeoutException.
        /// </summary>
        /// <param name="predict">Calling method</param>
        /// <param name="timeOutInterval">Max execution time. 0 = Infinite</param>
        public static bool ExecuteMethod(Action predict, int timeOutInterval,int retryCounter=1)
        {
            bool IsFinished = false;
            ObjectState objectState = new ObjectState();
            AutoResetEvent autoEvent = new AutoResetEvent(false);
            int timeOutInt = timeOutInterval > 0 ? timeOutInterval: Timeout.Infinite;
            objectState.autoEvent = autoEvent;
            objectState.predict = predict;
            Timer timer = null;

            var callbackMethod = new TimerCallback(TimerCallbackMethod);
            timer = new Timer(callbackMethod, objectState, 0, timeOutInt+5);
    

            try
            {
                for (int i = 0; i < retryCounter; i++)
                {
                    var isSignal = autoEvent.WaitOne(timeOutInt, false);
                    if (isSignal)
                     break; 
                    if (retryCounter - 1 == i) throw new TimeoutException();
                    else
                        timer.Change(0, timeOutInt);
                }
            }
            finally
            {
                IsFinished = true;

                if (autoEvent != null)
                    autoEvent.Dispose();
                if (timer != null)
                    timer.Dispose();
            }
            return IsFinished;
        }

        #region private methods
        private static void TimerCallbackMethod(object state)
        {
            var objectSate = (ObjectState)state;
            var predict = (Action)objectSate.predict;
            predict();
            if(objectSate!=null && !objectSate.autoEvent.SafeWaitHandle.IsClosed)
                objectSate.autoEvent.Set();
        }
        #endregion

    }
}
