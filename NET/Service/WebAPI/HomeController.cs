﻿using System.Text;
using System.Diagnostics;
using System.IO;
using System;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.ServiceModel.Web;
using Service.Helpers;

namespace Service
{
    // [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]

    public class HomeController :ApiController
    {
        HttpContext context;

        public HomeController()
        {
            context = HttpContext.Current;
        }

        #region Service Model Commands


        [HttpGet]
        [AllowAnonymous]
        [Route("index")]
        public HttpResponseMessage GetIndex()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetIndex method"));

            ResourceList resourceList = null;
            //string filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings[ServiceConstants.PSIAIndexRXml]);
            //using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            //{
            //    var serializer = new XmlSerializer(typeof(ResourceList));
            //    resourceList = serializer.Deserialize(fileStream) as ResourceList;
            //}
            //if (resourceList != null)
            //{
            //    //For index command return just the first level nodes (i.e. set inner ResourceList to null)
            //    resourceList.Resource.ToList().ForEach(x => x.ResourceList = null);
            //}
            return Utilities.GetXmlResponse<ResourceList>(resourceList, Request);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("indexr")]
        public HttpResponseMessage GetIndexr()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetIndexr method"));

            ResourceList resourceList = null;
            //string filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings[ServiceConstants.PSIAIndexRXml]);
            //using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            //{
            //    var serializer = new XmlSerializer(typeof(ResourceList));
            //    resourceList = serializer.Deserialize(fileStream) as ResourceList;
            //}
            return Utilities.GetXmlResponse<ResourceList>(resourceList, Request);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("description")]
        public HttpResponseMessage GetDescription()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetDescription method"));

            var response = new ResourceDescription
            {
                
                name = "PSIA PLAI Service",
                version = "1.0",
                description = "PSIA PLAI Service",
                type = ResourceType.resource,
            };

            return Utilities.GetXmlResponse<ResourceDescription>(response, Request);

        }

        #endregion
        #region private_methods


        private static string DeserealizeFile(string inputPath)
        {
            string xmlString = string.Empty;
            try
            {
                using (var streamReader = new StreamReader(new FileStream(inputPath, FileMode.Open, FileAccess.Read)))
                {
                    xmlString = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
         
            }
            return xmlString;
        }
        //This method is obselete. Would be removed.
        public static string GetIssuerSignatureFromHeader()
        {
            return WebOperationContext.Current.IncomingRequest.Headers[ServiceConstants.IssuerSignatureHeader];
        }
        /// <summary>
        /// This is obselete. Would throw exception.
        /// </summary>
        /// <returns></returns>
        public static string[] GetCredentialsFromHeaders()
        {
            string credentials = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
            if (credentials != null)
                credentials = credentials.Trim();

            if (!String.IsNullOrEmpty(credentials))
            {
                try
                {
                    string[] credentialParts = credentials.Split(new[] { ' ' });
                    if (credentialParts.Length == 2 && credentialParts[0].Equals("basic", StringComparison.OrdinalIgnoreCase))
                    {
                        credentials = Encoding.ASCII.GetString(Convert.FromBase64String(credentialParts[1]));
                        credentialParts = credentials.Split(new[] { ':' });
                        if (credentialParts.Length == 2)
                            return credentialParts;
                    }
                }
                catch (Exception ex)
                {
       
                }
            }

            return null;
        }

      



        #endregion

    }
}
