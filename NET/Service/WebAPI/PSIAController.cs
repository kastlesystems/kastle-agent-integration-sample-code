﻿using System.Text;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System;
using System.Net.Http;
using System.Net;
using System.Threading;
using System.Web.Http;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.ServiceModel.Web;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;
using PSIA.Common.Soap.Objects.Model;
using PSIA.Common.Soap.Objects;
using PSIA.CSEC;
using System.Web.Hosting;
using System.Configuration;
using System.Collections.Generic;
using Service.Model;
using Service.Helpers;


namespace Service
{
    // [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [RoutePrefix("PSIA")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PSIAController : ApiController
    {
        #region private_variables
        private static int TimerFreqMSec = 2000;        
        private static readonly Timer timer = new Timer(TimerCallback, null, 0, TimerFreqMSec);        
        private static StreamWriter writer;
        public static ConcurrentDictionary<string, AreaControlEvent> eventsCollection;        
        public static ConcurrentDictionary<string, DateTime> processingEvents = new ConcurrentDictionary<string, DateTime>();
        public static ConcurrentDictionary<string, FailedDetails> failedEvents = new ConcurrentDictionary<string, FailedDetails>();        
        private const string PERSONNEL_UDF_FILEDS = "UDF__PsiaCustomData_";
        private const string CLEARANCE_UDF_FILEDS = "UDF__PSIAClearance_";
        private static string SourceID;
        private static bool ClearMarkerForNoData = ConfigurationManager.AppSettings["ClearMarkerForNoData"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["ClearMarkerForNoData"]) : false;
        private static int ackLostReattemptDuration = Convert.ToInt32(ConfigurationSettings.AppSettings["AckLostReattemptDuration"]);
        private static int FldReattemptDurationForFstTwo = Convert.ToInt32(ConfigurationSettings.AppSettings["FldReattemptDurationForFstTwo"]);
        private static int FldReattemptDurationForAfterTwo = Convert.ToInt32(ConfigurationSettings.AppSettings["FldReattemptDurationForAfterTwo"]);

        #endregion
        #region public variables
        public static string IssuerSignature { get; set; }
        #endregion
        #region constructors
        static PSIAController()
        {            
            eventsCollection = new ConcurrentDictionary<string, AreaControlEvent>();
        }
        public PSIAController()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            SourceID = "{" + assembly.GetType().GUID.ToString() + "}";
        }
        #endregion
        #region Area Control commands
        #region System


        [HttpGet]
        [Route("profile")]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetPsiaProfile()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPsiaProfile method"));

            PsiaProfile profile = new PsiaProfile()
            {
                systemID = "{49A5540D-B62D-45EA-8225-95E0DDF42945}",
                nativeID = "{49A5540D-B62D-45EA-8225-95E0DDF42945}",
                psiaServiceVersion = 1.2F,


            };
            profile.version = "1.1";

            profile.primaryPsiaSpec = new PsiaSpecDecl();
            profile.primaryPsiaSpec.psiaSpecName = PsiaSpecTag.areaCtl;
            profile.primaryPsiaSpec.psiaSpecVersion = 3;
            profile.primaryPsiaSpec.psiaSpecProfile = PsiaSpecProfileLevel.core;
            profile.primaryPsiaSpec.psiaSpecProfileSpecified = false;

            profile.profileList = new PsiaProfileDecl[5];
            profile.profileList[0] = new PsiaProfileDecl();
            profile.profileList[0].psiaProfileName = "PLAIBase";
            profile.profileList[0].psiaProfileVersion = 1;
            profile.profileList[0].psiaSpec = PsiaSpecTag.areaCtl;

            profile.profileList[1] = new PsiaProfileDecl();
            profile.profileList[1].psiaProfileName = "PLAI-FunctionalRolesOption";
            profile.profileList[1].psiaProfileVersion = 1;
            profile.profileList[1].psiaSpec = PsiaSpecTag.areaCtl;

            profile.profileList[2] = new PsiaProfileDecl();
            profile.profileList[2].psiaProfileName = "PLAI-RawLocationOption";
            profile.profileList[2].psiaProfileVersion = 1;
            profile.profileList[2].psiaSpec = PsiaSpecTag.areaCtl;

            profile.profileList[3] = new PsiaProfileDecl();
            profile.profileList[3].psiaProfileName = "BaselineAccessControl";
            profile.profileList[3].psiaProfileVersion = 1;
            profile.profileList[3].psiaSpec = PsiaSpecTag.areaCtl;


            profile.profileList[4] = new PsiaProfileDecl();
            profile.profileList[4].psiaProfileName = "Intrusion-Control";
            profile.profileList[4].psiaProfileVersion = 1;
            profile.profileList[4].psiaSpec = PsiaSpecTag.areaCtl;


            return Utilities.GetXmlResponse<PsiaProfile>(profile, Request);
        }


        [Route("System/status")]
        [HttpGet]
        public HttpResponseMessage SystemStatus()
        {

            SystemLogger.WriteInformation(string.Format("Calling SystemStatus method"));

            DeviceStatus systemStatus = new DeviceStatus() { version = "1.0", currentDeviceTime = new DateTimeCap() { Value = DateTime.Now } };
            return Utilities.GetXmlResponse(systemStatus, Request);
        }


        [Route("System/time")]
        [HttpGet]
        public HttpResponseMessage SystemTime()
        {

            SystemLogger.WriteInformation(string.Format("Calling SystemTime method"));

            Time time = new Time() { version = "1.0", timeMode = new TimeTimeMode() { Value = "NTP" }, localTime = new DateTimeCap() { Value = DateTime.Now }, timeZone = new StringCap() { Value = TimeZone.CurrentTimeZone.StandardName } };
            return Utilities.GetXmlResponse(time, Request);

        }
        #endregion
        #region AreaControl
        [Route("AreaControl/configuration")]
        [HttpGet]
        public HttpResponseMessage GetConfuguration()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetConfuguration method"));

            AreaControlConfiguration configuration = GetConfugurationAction();
            return Utilities.GetXmlResponse(configuration, Request);



        }

        [Route("AreaControl/status")]
        [HttpGet]
        public HttpResponseMessage GetStatus()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetStatus method"));

            AreaControlStatus configuration = GetStatusAction();
            return Utilities.GetXmlResponse(configuration, Request);



        }


        #endregion
        #region Partitions

        [Route("AreaControl/Partitions/info")]
        [HttpGet]
        public HttpResponseMessage GetPartitionsInfo()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPartitionsInfo method"));

            PartitionInfoList partitions = GetPartitionsInfoList();
            return Utilities.GetXmlResponse(partitions, Request);

        }


        [Route("AreaControl/Partitions/info/{partitionID}")]
        [HttpGet]
        public HttpResponseMessage GetPartitionInfo(string partitionID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPartitionInfo method"));

            PartitionInfo partitions = GetPartitionInfoById(partitionID);
            return Utilities.GetXmlResponse(partitions, Request);

        }






        #endregion
        #region Permissions
        [Route("AreaControl/Permissions/info")]
        [HttpGet]
        public HttpResponseMessage GetPersmissions()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPersmissions method"));

            PermissionInfoList permmissions = GetPermissionsList();
            return Utilities.GetXmlResponse(permmissions, Request);



        }


        [Route("AreaControl/Permissions/info/{permissionID}")]
        [HttpGet]
        public HttpResponseMessage GetPersmission(string permissionID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPersmissions method"));

            PermissionInfo permmission = GetPermissionByID(permissionID);
            return Utilities.GetXmlResponse(permmission, Request);



        }



        [Route("AreaControl/Permissions/info")]
        [HttpPost]
        async public Task<HttpResponseMessage> PostPersmissions()
        {
            try
            {

                SystemLogger.WriteInformation(string.Format("Calling PostPersmissions method"));

                byte[] contents = await Request.Content.ReadAsByteArrayAsync();

                if (contents != null)
                {

                    PermissionInfo permissionInfo = Encoding.UTF8.GetString(contents).FromXml<PermissionInfo>();
                    if (permissionInfo != null)
                    {
                        var response = CreatePersmissionObject(permissionInfo);
                        return Utilities.GetXmlResponse(response, Request);
                    }
                }

                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }
            catch (Exception ex)
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }

        }

        [Route("AreaControl/Permissions/info/{PermissionID}")]
        [HttpPut]
        public HttpResponseMessage PutPersmission(string PermissionID, [FromBody] PermissionInfo permissionInfo)
        {
            try
            {

                SystemLogger.WriteInformation(string.Format("Calling PutPersmission method"));


                if (permissionInfo != null)
                {
                    var response = CreatePersmissionObject(permissionInfo);
                    return Utilities.GetXmlResponse(response, Request);
                }


                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }
            catch (Exception ex)
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }

        }


        #endregion
        #region Credentials




        [Route("AreaControl/Credentials/info")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetCredentials([FromUri] string credentialUID = null, [FromUri] string identifierValue = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentials method"));


            CredentialInfoList inputInfoList = GetCredentialsListInfo(credentialUID, identifierValue);
            return Utilities.GetXmlResponse(inputInfoList, Request);

        }


        [Route("AreaControl/Credentials/info")]
        [HttpPost]
        [BasicAuthenticationHandler]
        async public Task<HttpResponseMessage> PostCredentials()
        {



            SystemLogger.WriteInformation(string.Format("Calling PutCredentials method"));

            byte[] contents = await Request.Content.ReadAsByteArrayAsync();

            if (contents != null)
            {

                CredentialInfo credentialInfo = Encoding.UTF8.GetString(contents).FromXml<CredentialInfo>();
                if (credentialInfo != null)
                {
                    var response = CreateCredentialObject(credentialInfo);
                    return Utilities.GetXmlResponse(response, Request);
                }
            }

            var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
            return Utilities.GetXmlResponse(responseStatus, Request);
        }



        [Route("AreaControl/Credentials/info/{credentialUID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCredential(string credentialUID)
        {


            SystemLogger.WriteInformation(string.Format("Calling GetCredential method"));


            CredentialInfo credentialInfo = GetCredentialInfo(credentialUID);
            return Utilities.GetXmlResponse(credentialInfo, Request);
        }

        [Route("AreaControl/Credentials/info/{credentialUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCredentialInfo(string credentialUID, [FromBody] CredentialInfo credentialInfo)
        {


            SystemLogger.WriteInformation(string.Format("Calling PutCredentialInfo method"));

            var credentialInfoItem = credentialInfo;// Utilities.StreamToObject<CredentialInfo>(credentialInfo);
            if (credentialInfoItem != null)
            {
                var response = CreateCredentialObject(credentialInfoItem);
                return Utilities.GetXmlResponse(response, Request);
            }
            else
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object GUID: {0}", credentialUID));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }

        }

        [Route("AreaControl/CredentialTesting/info/{credentialUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCredentialInfoTest(string credentialUID, [FromBody] TestingStart credentialInfo)
        {


            SystemLogger.WriteInformation(string.Format("Calling PutCredentialInfo method"));

            //var credentialInfoItem = credentialInfo;// Utilities.StreamToObject<CredentialInfo>(credentialInfo);
            //if (credentialInfoItem != null)
            //{
            //    var response = CreateCredentialObject(credentialInfoItem);
            //    return Utilities.GetXmlResponse(response, Request);
            //}
            //else
            //{
            //    var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object GUID: {0}", credentialUID));
            //    return Utilities.GetXmlResponse(responseStatus, Request);
            //}
            return null;

        }
        [Route("AreaControl/Credentials/info/{credentialUID}")]
        [HttpDelete]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage DeleteCredentialInfo(string credentialUID)
        {


            SystemLogger.WriteInformation(string.Format("Calling DeleteCredentialInfo method"));

            var response = DeleteCredentialById(credentialUID);
            return Utilities.GetXmlResponse(response, Request);

        }

        [Route("AreaControl/Credentials/state/{credentialUID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCredentialState(string credentialUID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentialState method"));



            CredentialState response = GetCredentialStateById(credentialUID);

            return Utilities.GetXmlResponse(response, Request);

        }

        [Route("AreaControl/Credentials/state/{credentialUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCredentialState(string credentialUID, [FromUri]string activeState)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentialState method"));



            ResponseStatus response = PutCredentialObjectState(credentialUID, activeState);
            return Utilities.GetXmlResponse(response, Request);

        }



        #endregion
        #region CardFormats




        [Route("AreaControl/CredentialFormats/info")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetCardFormats()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCardFormats method"));


            CardFormatInfoList inputInfoList = GetCardFormatListInfo();
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/CredentialFormats/info")]
        [HttpPost]
        [BasicAuthenticationHandler]
        async public Task<HttpResponseMessage> PostCardFormat()
        {

            WiegandCardFormatInfo wiegandcardFormatInfo = null;
            MagstripeCardFormatInfo magstripeCardFormatInfo = null;


            SystemLogger.WriteInformation(string.Format("Calling PostCardFormat method"));

            byte[] contents = await Request.Content.ReadAsByteArrayAsync();

            if (contents != null)
            {

                try
                {
                    wiegandcardFormatInfo = Encoding.UTF8.GetString(contents).FromXml<WiegandCardFormatInfo>();
                }
                catch (Exception ex)
                {

                    SystemLogger.WriteInformation(ex.ToString());
                }

                if (wiegandcardFormatInfo != null)
                {
                    var response = CreateWiegandCardFormatObject(wiegandcardFormatInfo);
                    return Utilities.GetXmlResponse(response, Request);
                }
                else
                {
                    try
                    {

                        magstripeCardFormatInfo = Encoding.UTF8.GetString(contents).FromXml<MagstripeCardFormatInfo>();
                    }
                    catch (Exception ex)
                    {

                        SystemLogger.WriteInformation(ex.ToString());
                    }

                    if (magstripeCardFormatInfo != null)
                    {
                        var response = CreateMagstripeCardFormatObject(magstripeCardFormatInfo);
                        return Utilities.GetXmlResponse(response, Request);
                    }
                }
            }

            var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object"));
            return Utilities.GetXmlResponse(responseStatus, Request);
        }



        [Route("AreaControl/CredentialFormats/info/{cardFormatUID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCardFormat(string cardFormatUID)
        {


            SystemLogger.WriteInformation(string.Format("Calling GetCardFormat method"));


            WiegandCardFormatInfo wiegandcardFormatInfo = GetWiegandCardFormatInfo(cardFormatUID);
            if (wiegandcardFormatInfo != null)
            {
                return Utilities.GetXmlResponse(wiegandcardFormatInfo, Request);
            }
            else
            {
                MagstripeCardFormatInfo magstripeCardFormatInfo = GetMagstripeCardFormatInfo(cardFormatUID);
                if (magstripeCardFormatInfo != null)
                {
                    return Utilities.GetXmlResponse(magstripeCardFormatInfo, Request);
                }
            }

            return null;
        }



        [Route("AreaControl/CredentialFormats/info/{cardFormatUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCardFormatInfo(string cardFormatUID)
        {
            MagstripeCardFormatInfo magstripeCardFormatInfo = null;
            WiegandCardFormatInfo wiegandcardFormatInfo = null;


            SystemLogger.WriteInformation(string.Format("Calling PutCardFormatInfo method"));

            string result = Request.Content.ReadAsStringAsync().Result;

            try
            {
                magstripeCardFormatInfo = result.FromXml<MagstripeCardFormatInfo>();
            }
            catch (Exception ex)
            {

                SystemLogger.WriteInformation(ex.ToString());
            }

            if (magstripeCardFormatInfo == null)
            {
                try
                {
                    wiegandcardFormatInfo = result.FromXml<WiegandCardFormatInfo>();
                }
                catch (Exception ex)
                {

                    SystemLogger.WriteInformation(ex.ToString());
                }
            }
            else
            {
                var response = CreateMagstripeCardFormatObject(magstripeCardFormatInfo);
                return Utilities.GetXmlResponse(response, Request);
            }

            if (wiegandcardFormatInfo == null)
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object GUID: {0}", cardFormatUID));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }
            else
            {
                var response = CreateWiegandCardFormatObject(wiegandcardFormatInfo);
                return Utilities.GetXmlResponse(response, Request);
            }



        }

        [Route("AreaControl/CredentialFormats/info/{cardFormatUID}")]
        [HttpDelete]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage DeleteCardFormatInfo(string cardFormatUID)
        {


            SystemLogger.WriteInformation(string.Format("Calling DeleteCardFormatInfo method"));

            var response = DeleteCardFormatById(cardFormatUID);
            return Utilities.GetXmlResponse(response, Request);

        }




        #endregion
        #region CardHolders


        /// <summary>
        /// Gets the credential holder information.
        /// </summary>
        /// <param name="cardholderlUID">The credential holder unique identifier.</param>
        /// <returns></returns>
        [Route("AreaControl/CredentialHolders/info/{cardholderlUID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCredentialCardHolder(string cardholderlUID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentialCardHolder method"));

            var holder = GetCredentialHolderObject(cardholderlUID);
            if (holder != null)
            {

                return Utilities.GetXmlResponse(holder, Request);
            }

            return null;
        }


        [Route("AreaControl/CredentialHolders/info")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCredentialCardHolders()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Puts the credential holder information.
        /// </summary>
        /// <param name="credentialHolderUID">The credential holder unique identifier.</param>
        /// <param name="credentialHolderInfo">The credential holder information.</param>
        /// <returns></returns>
        [Route("AreaControl/CredentialHolders/info/{credentialHolderUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCredentialHolderInfo(string credentialHolderUID, [FromBody] CredentialHolderInfo credentialHolderInfo)
        {


            SystemLogger.WriteInformation(string.Format("Calling PutCredentialHolderInfo method"));

            var credentialHolderInfoItem = credentialHolderInfo; //Utilities.StreamToObject<CredentialHolderInfo>(credentialHolderInfo);

            if (credentialHolderInfoItem != null)
            {
                //   var status = UpdatePersonnelObject(credentialHolderInfoItem);
                var response = CreateCardHolderObject(credentialHolderInfoItem);
                return Utilities.GetXmlResponse(response, Request);
            }
            else
            {
                var response = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidXMLFormat);
                return Utilities.GetXmlResponse(response, Request);
            }
        }


        /// <summary>
        /// Deletes the credential holder information.
        /// </summary>
        /// <param name="credentialHolderUID">The credential holder unique identifier.</param>
        /// <returns></returns>
        [Route("AreaControl/CredentialHolders/info/{credentialHolderUID}")]
        [HttpDelete]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage DeleteCredentialHolderInfo(string credentialHolderUID)
        {

            SystemLogger.WriteInformation(string.Format("Calling DeleteCredentialHolderInfo method"));

            var response = DeleteCardHolderObject(credentialHolderUID);
            return Utilities.GetXmlResponse(response, Request);
        }


        [Route("PSIA/AreaControl/CredentialHolders/state/{credentialHolderUID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutCredentialHolderrState(string credentialHolderUID, [FromUri] string activeState)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutCardHolderState method"));


            ResponseStatus response = PutCardHolderObjectState(credentialHolderUID, activeState);
            return Utilities.GetXmlResponse(response, Request);

        }

        /// <summary>
        /// Gets the credential holder state
        /// </summary>
        /// <param name="credentialHolderUID">The Credential Holder unique identifier.</param>
        /// <returns></returns>
        [Route("AreaControl/CredentialHolders/state/{credentialHolderUID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetCredentialHolderState(string credentialHolderUID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentialHolderState method"));



            CredentialHolderState response = GetCredentialHolderObjectState(credentialHolderUID);
            return Utilities.GetXmlResponse(response, Request);
        }



        #endregion
        #region Roles
        [Route("AreaControl/Roles/info/{roleUid}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetRole(string roleUid)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetCredentials method"));


            RoleInfo credentialInfo = GetRoleInfo(roleUid);
            return Utilities.GetXmlResponse(credentialInfo, Request);
        }



        [Route("AreaControl/Roles/info/{roleUid}")]
        [HttpDelete]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage DeleteRole(string roleUid)

        {

            SystemLogger.WriteInformation(string.Format("Calling DeleteRole method"));

            var response = DeleteRolelById(roleUid);
            return Utilities.GetXmlResponse(response, Request);
        }



        [Route("AreaControl/Roles/info/{roleUid}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutRole(string roleUid, RoleInfo roleInfo)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutRole method"));

            var roleInfoItem = roleInfo;
            if (roleInfoItem != null)
            {
                var response = CreateRoleObject(roleInfoItem);
                return Utilities.GetXmlResponse(response, Request);
            }
            else
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object GUID: {0}", roleUid));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }
        }


        #endregion
        #region Outputs
        [Route("AreaControl/Devices/Outputs/info")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetOutputsInfo([FromUri] bool externalOnly = false, [FromUri] string portalID = null, [FromUri] string zoneID = null, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetOutputsInfo method"));


            OutputInfoList outputInfoList = GetOutputsInfoList();
            return Utilities.GetXmlResponse(outputInfoList, Request);
        }

        [Route("AreaControl/Devices/Outputs/status")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetOutputsStatus([FromUri] bool externalOnly = false, [FromUri] string portalID = null, [FromUri] string zoneID = null, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetOutputsStatus method"));


            OutputStatusList outputStatusInfoList = GetOutputsStatusInfoList();
            return Utilities.GetXmlResponse(outputStatusInfoList, Request);
        }


        [Route("AreaControl/Devices/Outputs/instanceDescription/{outputID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetOutputDescription(string outputID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetOutputDescription method"));


            OutputDescriptionInfo outputDescription = GetOutputDescriptionByID();
            return Utilities.GetXmlResponse(outputDescription, Request);
        }


        [Route("AreaControl/Devices/Outputs/instanceDescription/{outputID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutOutputDescription(string outputID, [FromBody] OutputDescriptionInfo outputDescription)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutOutputDescription method"));


            ResponseStatus response = PutOutputDescriptionbyID(outputID, outputDescription);
            return Utilities.GetXmlResponse(response, Request);
        }


        [Route("AreaControl/Devices/Outputs/state/{outputID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutOutputState(string outputID, [FromUri] string state)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutOutputState method"));


            ResponseStatus response = PutOutputStateID(outputID, state);
            return Utilities.GetXmlResponse(response, Request);
        }


        [Route("AreaControl/Devices/Outputs/status/{outputID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetOutputStatus(string outputID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetOutputStatus method"));


            OutputStatus outputInfo = GetOutputStatusById();
            return Utilities.GetXmlResponse(outputInfo, Request);
        }


        #endregion
        #region Inputs
        [Route("AreaControl/Devices/Inputs/info")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetInputsInfo([FromUri] bool externalOnly = false, [FromUri] string portalID = null, [FromUri] string zoneID = null, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetInputsInfo method"));


            InputInfoList inputInfoList = GetInputsInfoList(portalID, zoneID, partitionID);
            var response = Utilities.GetXmlResponse(inputInfoList, Request);
            return response;
        }

        [Route("AreaControl/Devices/Inputs/status")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetInputsStatus([FromUri] bool externalOnly = false, [FromUri] string portalID = null, [FromUri] string zoneID = null, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetInputsStatus method"));


            InputStatusList inputInfoList = GetInputsStatusList(portalID, zoneID, partitionID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/Devices/Inputs/status/{inputID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetInputStatus(string inputID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetInputStatus method"));


            InputStatus inputInfo = GetInputStatusById();
            return Utilities.GetXmlResponse(inputInfo, Request);
        }



        [Route("AreaControl/Devices/Inputs/instanceDescription/{inputID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetInputDescription(string inputID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetInputDescription method"));


            InputDescriptionInfo outputDescription = GetInputDescriptionByID(inputID);
            return Utilities.GetXmlResponse(outputDescription, Request);
        }


        [Route("AreaControl/Devices/Inputs/instanceDescription/{inputID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutInputDescription(string inputID, [FromBody] InputDescriptionInfo inputDescription)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutInputDescription method"));


            ResponseStatus response = PutInputDescriptionbyID(inputID, inputDescription);
            return Utilities.GetXmlResponse(response, Request);
        }


        [Route("AreaControl/Devices/Inputs/maskState/{inputID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutInputMaskState(string inputID, [FromUri] string state)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutInputMaskState method"));


            ResponseStatus response = PutInputMaskStateByID(inputID, state);
            return Utilities.GetXmlResponse(response, Request);
        }



        [Route("AreaControl/Devices/Inputs/maskState")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutInputsMaskState([FromUri] string state)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutInputsMaskState method"));


            ResponseStatus response = PutInputsListMaskState(state);
            return Utilities.GetXmlResponse(response, Request);
        }


        #endregion
        #region Portals ( Door/Reader)

        [Route("AreaControl/PartitionMembers/Portals/accessOverride")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutPortalsAccessOverride([FromUri] string accessOverrideState, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalsAccessOverride method"));


            ResponseStatus inputInfoList = PutPortalListAccessOverride(accessOverrideState, partitionID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }

        [Route("AreaControl/PartitionMembers/Portals/accessOverride/{portalID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutPortalAccessOverride([FromUri] string accessOverrideState, [FromUri] string portalID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalAccessOverride method"));


            ResponseStatus inputInfoList = PutPortalAccessOverrideInfo(accessOverrideState, portalID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/PartitionMembers/Portals/status")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetPortalStatus([FromUri] string portalID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPortalStatus method"));


            PortalStatusList portalsStatusInfoList = GetPortalsStatusInfo(portalID);
            return Utilities.GetXmlResponse(portalsStatusInfoList, Request);
        }


        [Route("AreaControl/PartitionMembers/Portals/requiredIdentifierTypes/{portalID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetRequiredIdentifierTypes(string portalID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetRequiredIdentifierTypes method"));


            RequiredIdentifierList requiredIdentifierInfo = GetRequiredIdentifierTypesInfo(portalID);
            return Utilities.GetXmlResponse(requiredIdentifierInfo, Request);
        }


        [Route("AreaControl/PartitionMembers/Portals/requiredIdentifierTypes/{portalID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutRequiredIdentifierType(string portalID, [FromBody] RequiredIdentifierList requiredIdentifierInfoList)
        {


            SystemLogger.WriteInformation(string.Format("Calling PutRequiredIdentifierType method"));

            var requiredIdentifierInfoItem = requiredIdentifierInfoList;
            if (requiredIdentifierInfoItem != null)
            {
                var response = Utilities.GetResponseStatus(1, ActionStatusCode.OK);
                return Utilities.GetXmlResponse(response, Request);
            }
            else
            {
                var responseStatus = Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, string.Format("Unable to save object GUID: {0}", portalID));
                return Utilities.GetXmlResponse(responseStatus, Request);
            }

        }


        [Route("AreaControl/PartitionMembers/Portals/forcedMaskState")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutPortalsForcedMaskState([FromUri] string state, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalsForcedMaskState method"));


            ResponseStatus inputInfoList = PutPortalListForcedMaskState(state, partitionID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/PartitionMembers/Portals/info")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetPortalsInfo([FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPortalsInfo method"));


            PortalInfoList inputInfoList = GetPortalsInfoList(partitionID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }




        [Route("AreaControl/PartitionMembers/Portals/instanceDescription/{portalID}")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetPortalsInstanceDescription(string portalID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPortalsInstanceDescription method"));


            PortalDescriptionInfo inputInfoList = GetPortalsListInstanceDescription(portalID);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }




        [Route("AreaControl/PartitionMembers/Portals/instanceDescription/{portalID}")]
        [HttpPut]
        [BasicAuthenticationHandler]
        public HttpResponseMessage PutPortalsInstanceDescription(string portalID, [FromBody] PortalDescriptionInfo portalDescriptionInfo)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalsInstanceDescription method"));


            ResponseStatus inputInfoList = PutPortalsListInstanceDescription(portalID, portalDescriptionInfo);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }


        [Route("AreaControl/PartitionMembers/Portals/latchState")]
        [HttpPut]
        [BasicAuthenticationHandler]
        public HttpResponseMessage PutPortalsLatchState([FromUri] string state, [FromBody] PulseData portalPulse, [FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalsLatchState method"));


            ResponseStatus inputInfoList = PutPortalsListLatchState(partitionID, state, portalPulse);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/PartitionMembers/Portals/latchState/{portalID}")]
        [HttpPut]
        [BasicAuthenticationHandler]
        public HttpResponseMessage PutPortalLatchState(string portalID, [FromUri] string state, [FromBody] PulseData portalPulse)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutPortalsInstanceDescription method"));


            ResponseStatus inputInfoList = PutPortalsLatchStateByID(portalID, state, portalPulse);
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }



        [Route("AreaControl/PartitionMembers/Portals/status/{portalID}")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetPortalsStatus([FromUri] string portalID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetPortalsStatus method"));


            PortalStatus inputInfoList = GetPortalStatusById();
            return Utilities.GetXmlResponse(inputInfoList, Request);
        }


        #endregion
        #region Zones
        [Route("AreaControl/PartitionMembers/Zones/info")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetZonesInfo([FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetZonesInfo method"));


            ZoneInfoList zoneInfoList = GetZonesInfoList(partitionID);
            return Utilities.GetXmlResponse(zoneInfoList, Request);
        }

        [Route("AreaControl/PartitionMembers/Zones/info/{zoneID}")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetZoneInfo(string zoneID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetZoneInfo method"));


            ZoneInfo zoneInfo = GetZoneInfoById(zoneID);
            return Utilities.GetXmlResponse(zoneInfo, Request);
        }



        [Route("AreaControl/PartitionMembers/Zones/status")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetZonesStatus([FromUri] string partitionID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetZonesStatus method"));


            ZoneStatusList zoneInfoList = GetZonesStatusInfoList(partitionID);
            return Utilities.GetXmlResponse(zoneInfoList, Request);
        }



        [Route("AreaControl/PartitionMembers/Zones/status/{zoneID}")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetZoneStatus(string zoneID = null)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetZoneStatus method"));


            ZoneStatus zoneInfo = GetZoneStatusById();
            return Utilities.GetXmlResponse(zoneInfo, Request);
        }


        [Route("AreaControl/PartitionMembers/Zones/instanceDescription/{zoneID}")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetZoneDescription(string zoneID)
        {

            SystemLogger.WriteInformation(string.Format("Calling GetZoneDescription method"));


            ZoneDescriptionInfo zoneDescription = GetZoneDescriptionByID(zoneID);
            return Utilities.GetXmlResponse(zoneDescription, Request);
        }



        [Route("AreaControl/PartitionMembers/Zones/instanceDescription/{zoneID}")]
        [HttpPut]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage PutZoneDescription(string zoneID, [FromBody] ZoneDescriptionInfo zoneDescription)
        {

            SystemLogger.WriteInformation(string.Format("Calling PutZoneDescription method"));


            ResponseStatus response = PutZoneDescriptionbyID(zoneID, zoneDescription);
            return Utilities.GetXmlResponse(response, Request);
        }



        #endregion

        #endregion
        #region CSEC

        /// <summary>
        /// Gets the specified owner unique identifier.
        /// </summary>
        /// <param name="ownerGUID">The owner unique identifier.</param>
        /// <param name="ExpireTime">The expire time.</param>
        /// <returns></returns>
        [Route("CSEC/deviceOwnership")]
        [HttpGet]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage GetDeviceOwnership([FromUri]string OwnerGUID, [FromUri]string ExpireTime = "")
        {


            SystemLogger.WriteInformation(string.Format("Calling GetDeviceOwnership method"));

            string[] credentials = GetCredentialsFromHeaders(Request.Headers);


            if (credentials != null && credentials.Length > 0)
                IssuerSignature = Utilities.GetSha256Hash(OwnerGUID + ":" + credentials[0] + ":" + credentials[1]);
            var randomizer = new Random();

            var csecOwnershipCookie = new CSECOwnershipCookie
            {


                ownerCode = randomizer.Next().ToString("x"),
                version = "1.0",
                cookieVersion = "1.0",
                expires = DateTime.MaxValue.ToString(ServiceConstants.DateTimeFormat),
                path = "/PSIA",
                issuerSignature = IssuerSignature
            };

            return Utilities.GetXmlResponse<CSECOwnershipCookie>(csecOwnershipCookie, Request);
        }

        /// <summary>
        /// Resets the device ownership.
        /// </summary>
        /// <param name="ResetOwnership">The reset ownership.</param>
        /// <returns></returns>
        [Route("CSEC/deviceOwnership")]
        [HttpDelete]
        [BasicAuthenticationHandlerAttribute]
        public HttpResponseMessage ResetDeviceOwnership([FromUri] bool ResetOwnership = true)
        {
            SystemLogger.WriteInformation(string.Format("Calling ResetDeviceOwnership method"));
            IssuerSignature = null;
            var response = Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            return Utilities.GetXmlResponse<ResponseStatus>(response, Request);
        }

        #endregion
        #region CMEM Commands
        /// <summary>
        /// For establishing the stream.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("Metadata/stream")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetMetaDataStream(HttpRequestMessage request)
        {
            string boundaryConfig = ConfigurationSettings.AppSettings["BoundarySetting"];
            var response = request.CreateResponse();
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Cache-Control", "no-cache, must-revalidate");
            response.Headers.Add("ContentType", "multipart/mixed, boundary=" + boundaryConfig);
            response.Content = new PushStreamContent((Action<Stream, HttpContent, TransportContext>)OnStreamAvailable, "text/event-stream");
            response.Content.Headers.Remove("Content-Type");
            response.Content.Headers.TryAddWithoutValidation("Content-Type", "multipart/mixed; boundary=" + boundaryConfig);
            return response;
        }

        /// <summary>
        /// For Acknowledgement of the XMLs processed by PLAI Agent.
        /// </summary>
        /// <param name="metaLink">The MetaLink UID</param>
        /// <param name="ackResponse">The response after processing the XML.</param>
        /// <returns></returns>
        [Route("Metadata/Ack/{metaLink}")]
        [HttpPut]
        [BasicAuthenticationHandler]
        public HttpResponseMessage PutAcknowledgement(string metaLink, ResponseStatus ackResponse)
        {
            try
            {                
                if (ackResponse.statusCode == 1)
                {
                    SystemLogger.WriteInformation("PutAcknowledgement: Success Status Code Received.");
                    FailedDetails val;
                    DateTime value;
                    processingEvents.TryRemove(metaLink, out value);
                    failedEvents.TryRemove(metaLink, out val);
                    SystemLogger.WriteInformation(string.Format("PutAcknowledgement: Entry removed for metalink: {0}", metaLink));
                    Console.WriteLine(string.Format("PutAcknowledgement: Entry removed for metalink: {0}", metaLink));
                }
                else
                {
                    SystemLogger.WriteInformation("PutAcknowledgement: Error Status Code Received.");
                    DateTime value;
                    processingEvents.TryRemove(metaLink, out value);
                    if (failedEvents.ContainsKey(metaLink))
                    {
                        failedEvents[metaLink].FailedCount++;
                        failedEvents[metaLink].FailedTimeStamp = DateTime.Now;
                        SystemLogger.WriteInformation("PutAcknowledgement: Updated in failed with metalink: " + metaLink + " FailedDetails: " + failedEvents[metaLink].FailedCount + " | " + failedEvents[metaLink].FailedTimeStamp);
                        Console.WriteLine("PutAcknowledgement: Updated in failed with metalink: " + metaLink + " FailedDetails: " + failedEvents[metaLink].FailedCount + " | " + failedEvents[metaLink].FailedTimeStamp);
                    }
                    else
                    {                        
                        FailedDetails failedDetails = new FailedDetails
                        {
                            FailedCount = 0,
                            FailedTimeStamp = DateTime.Now
                        };
                        failedEvents.TryAdd(metaLink, failedDetails);
                        SystemLogger.WriteInformation("PutAcknowledgement: Added in failed with metalink: " + metaLink + " FailedDetails: " + failedDetails.FailedCount + " | " + failedDetails.FailedTimeStamp);
                        Console.WriteLine("PutAcknowledgement: Added in failed with metalink: " + metaLink + " FailedDetails: " + failedDetails.FailedCount + " | " + failedDetails.FailedTimeStamp);
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                SystemLogger.WriteError("PutAcknowledgement: Exception: " + ex);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


        [Route("Metadata/sessionSupport")]
        [HttpGet]
        [BasicAuthenticationHandler]
        public HttpResponseMessage GetMetaDataSessionSupport()
        {

            SystemLogger.WriteInformation(string.Format("Calling GetMetaDataSessionSupport method"));

            var metaFormat = new MetaFormat[1];
            metaFormat[0] = MetaFormat.xmlpsia;
            var sessionType = new SessionType[1];
            sessionType[0] = new SessionType()
            {
                metaSessionProtocol = SessionProtocolType.RESTSyncSessionTargetSend,
                metaSessionFlowType = SessionFlowType.datastream
            };
            var sessionSupport = new MetaSessionSupport()
            {
                metaFormats = metaFormat,
                metaSessionTypes = sessionType,
                multicastCapable = false,
                scheduleCapable = false,
                version = "2.0"
            };
            return Utilities.GetXmlResponse<MetaSessionSupport>(sessionSupport, Request);
        }

        #endregion
        #region private_methods

        #region Portals
        private ResponseStatus PutPortalAccessOverrideInfo(string accessOverrideState, string portalID)
        {



            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus PutPortalListAccessOverride(string accessOverrideState, string partitionID)
        {


            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus PutPortalListForcedMaskState(string state, string partitionID)
        {


            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }

        }
        private ResponseStatus PutPortalsListInstanceDescription(string portalID, PortalDescriptionInfo portalDescriptionInfo)
        {
            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }

        }
        private ResponseStatus PutPortalsListLatchState(string partitionID, string state, PulseData portalPulse)
        {


            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }

        }
        private ResponseStatus PutPortalsLatchStateByID(string portalID, string state, PulseData portalPulse)
        {

            SystemLogger.WriteInformation("Calling  PutPortalsListLatchState");

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private PortalDescriptionInfo GetPortalsListInstanceDescription(string portalID)
        {
            PortalDescriptionInfo portaDescription = null;
            portaDescription = new PortalDescriptionInfo();
            portaDescription.version = "1.0";
            portaDescription.Description = "PortalDescription";
            portaDescription.Name = "PortalName";



            return portaDescription;
        }
        private RequiredIdentifierList GetRequiredIdentifierTypesInfo(string partitionID)
        {

            RequiredIdentifierList requiredIdentifierList = null;
            requiredIdentifierList = new RequiredIdentifierList();
            requiredIdentifierList.version = "1.0";
            requiredIdentifierList.LogicalOr = new RequiredIdentifierInfo[1];
            requiredIdentifierList.LogicalOr[0] = GetRequiredIdentifier();
            return requiredIdentifierList;
        }
        private PortalStatusList GetPortalsStatusInfo(string partitionID)
        {

            PortalStatusList portalsList = null;
            portalsList = new PortalStatusList();
            portalsList.version = "1.0";



            portalsList.PortalStatus = new PortalStatus[1];
            portalsList.PortalStatus[0] = GetPortalStatusDetails();
            return portalsList;
        }
        private PortalStatus GetPortalStatusById()
        {
            PortalStatus portaStatus = null;
            portaStatus = GetPortalStatusDetails();
            return portaStatus;
        }
        private PortalInfoList GetPortalsInfoList(string partitionID)
        {
            PortalInfoList portalList = null;

            portalList = new PortalInfoList();
            portalList.PortalInfo = new PortalInfo[1];
            portalList.version = "1.0";
            portalList.PortalInfo[0] = new PortalInfo();
            portalList.PortalInfo[0].ID = 1;
            portalList.PortalInfo[0].UID = "{" + Guid.NewGuid().ToString() + "}";
            portalList.PortalInfo[0].Name = "PortalName";
            portalList.PortalInfo[0].Description = "PortalDescription";
            portalList.PortalInfo[0].version = "1.0";
            portalList.PortalInfo[0].AssociatedPartitionID = new ReferenceID() { ID = 1, GUID = "{" + Guid.NewGuid().ToString() + "}" };
            return portalList;
        }
        #endregion
        #region Zones
        private ZoneInfoList GetZonesInfoList(string partitionID)
        {
            ZoneInfoList zoneList = null;
            zoneList = new ZoneInfoList();
            zoneList.ZoneInfo = new ZoneInfo[1];
            zoneList.version = "1.0";
            zoneList.ZoneInfo[0] = GetZoneInfoDetails();
            return zoneList;
        }
        private ZoneInfo GetZoneInfoById(string zoneID)
        {
            ZoneInfo zoneinfo = null;
            SystemLogger.WriteInformation("Calling  GetZoneInfoAction");
            zoneinfo = GetZoneInfoDetails();
            return zoneinfo;
        }
        private ZoneStatusList GetZonesStatusInfoList(string partitionID)
        {
            ZoneStatusList zoneList = null;
            SystemLogger.WriteInformation("Calling  GetZonesStatusAction");

            zoneList = new ZoneStatusList();
            zoneList.ZoneStatus = new ZoneStatus[1];
            zoneList.version = "1.0";
            zoneList.ZoneStatus[0] = GetZoneStatusDetails();
            return zoneList;
        }
        private ZoneStatus GetZoneStatusById()
        {
            ZoneStatus zoneStatus = null;
            SystemLogger.WriteInformation("Calling  GetZoneStatusAction");
            zoneStatus = GetZoneStatusDetails();
            return zoneStatus;
        }
        private ZoneDescriptionInfo GetZoneDescriptionByID(string zoneID)
        {

            SystemLogger.WriteInformation("Calling  GetZoneDescriptionByID");

            ZoneDescriptionInfo zoneDescription = null;
            zoneDescription = new ZoneDescriptionInfo();
            zoneDescription.Name = "ZoneName";
            zoneDescription.Description = "ZoneDescription";
            return zoneDescription;
        }
        private ResponseStatus PutZoneDescriptionbyID(string zoneID, ZoneDescriptionInfo zoneDescription)
        {

            SystemLogger.WriteInformation("Calling  PutZoneDescriptionbyID");

            try
            {

                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private static ZoneInfo GetZoneInfoDetails()
        {
            ZoneInfo zoneinfo = new ZoneInfo();
            zoneinfo.ID = 1;
            zoneinfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            zoneinfo.Name = "ZoneName";
            zoneinfo.Description = "ZoneDescription";
            zoneinfo.version = "1.0";
            zoneinfo.Type = ZoneType.Intrusion;
            zoneinfo.ArmingType = ZoneArmingType.Interior;
            return zoneinfo;
        }
        private static ZoneStatus GetZoneStatusDetails()
        {
            ZoneStatus zoneStatus = new ZoneStatus();
            zoneStatus.ID = 1;
            zoneStatus.UID = "{" + Guid.NewGuid().ToString() + "}";
            zoneStatus.version = "1.0";
            zoneStatus.IntrusionAlarmSpecified = true;
            zoneStatus.IntrusionAlarm = IntrusionAlarmState.Alarm;
            zoneStatus.IntrusionAlarmTypeSpecified = true;
            zoneStatus.IntrusionAlarmType = IntrusionAlarmType.Intrusion;
            return zoneStatus;
        }
        #endregion
        #region Outputs
        private OutputInfoList GetOutputsInfoList(bool externalOnly = false, string portalID = null, string zoneID = null, string partitionID = null)
        {


            SystemLogger.WriteInformation("Calling  GetOutputsListInfo");
            OutputInfoList outputList = null;
            outputList = new OutputInfoList();
            outputList.version = "1.0";
            outputList.OutputInfo = new OutputInfo[1];

            outputList.OutputInfo[0] = new OutputInfo();
            outputList.OutputInfo[0].ID = 1;
            outputList.OutputInfo[0].UID = "{" + Guid.NewGuid().ToString() + "}";
            outputList.OutputInfo[0].IsExternal = false;
            outputList.OutputInfo[0].Name = "OutputName";
            outputList.OutputInfo[0].Description = "OutputDescription";
            outputList.OutputInfo[0].version = "1.0";
            outputList.OutputInfo[0].OutputInfoExtension = new AreaControlExtension[2];
            return outputList;
        }
        private OutputStatusList GetOutputsStatusInfoList()
        {

            SystemLogger.WriteInformation("Calling  GetOutputsStatusListInfo");

            OutputStatusList outputStatusList = null;
            outputStatusList = new OutputStatusList();
            outputStatusList.version = "1.0";
            outputStatusList.OutputStatus = new OutputStatus[1];
            outputStatusList.OutputStatus[0] = GetOutputStatusDetails();
            return outputStatusList;
        }
        private OutputStatus GetOutputStatusById()
        {
            OutputStatus outputStatus = null;
            outputStatus = GetOutputStatusDetails();
            return outputStatus;
        }
        private OutputDescriptionInfo GetOutputDescriptionByID()
        {

            SystemLogger.WriteInformation("Calling  GetOutputDescriptionByID");

            OutputDescriptionInfo outDescription = null;


            outDescription = new OutputDescriptionInfo();
            outDescription.version = "1.0";
            outDescription.Name = "OutputDescriptionName";
            outDescription.Description = "OutputDescription";
            return outDescription;
        }
        private ResponseStatus PutOutputStateID(string outputID, string state)
        {

            SystemLogger.WriteInformation("Calling  PutOutputStateID");

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }

        }
        private ResponseStatus PutOutputDescriptionbyID(string outputID, OutputDescriptionInfo outputDescription)
        {

            SystemLogger.WriteInformation("Calling  PutOutputDescriptionbyID");

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private static OutputStatus GetOutputStatusDetails()
        {
            OutputStatus outputStatus = new OutputStatus();
            outputStatus.ID = 1;
            outputStatus.UID = "{" + Guid.NewGuid().ToString() + "}";

            outputStatus.Connection = ConnectionState.OK;
            outputStatus.ConnectionSpecified = true;
            outputStatus.Output = OutputState.On;
            outputStatus.OutputSpecified = true;
            outputStatus.version = "1.0";
            return outputStatus;
        }
        #endregion
        #region Inputs
        private InputInfoList GetInputsInfoList(string portalID = null, string zoneID = null, string partitionID = null)
        {
            InputInfoList inputList = null;
            SystemLogger.WriteInformation("Calling  GetInputsListInfo");
            inputList = new InputInfoList();
            inputList.InputInfo = new InputInfo[1];
            inputList.version = "1.0";
            inputList.InputInfo[0] = new InputInfo();
            inputList.InputInfo[0].ID = 1;
            inputList.InputInfo[0].UID = "{" + Guid.NewGuid().ToString() + "}";
            inputList.InputInfo[0].IsExternal = false;
            inputList.InputInfo[0].IsExternalSpecified = true;
            inputList.InputInfo[0].Name = "InputName";
            inputList.InputInfo[0].Description = "InputDescription";
            inputList.InputInfo[0].version = "1.0";
            inputList.InputInfo[0].PortalID = new ReferenceID() { ID = 1, GUID = "{" + Guid.NewGuid().ToString() + "}" };
            return inputList;
        }
        private InputStatusList GetInputsStatusList(string portalID, string zoneID, string partitionID)
        {
            InputStatusList inputList = null;
            SystemLogger.WriteInformation("Calling  GetInputsListInfo");
            inputList = new InputStatusList();
            inputList.version = "1.0";
            inputList.InputStatus = new InputStatus[1];
            inputList.InputStatus[0] = GetInputStatusDetails();

            return inputList;
        }
        private InputStatus GetInputStatusById()
        {
            InputStatus inputStatus = null;
            inputStatus = GetInputStatusDetails();
            return inputStatus;
        }
        private InputDescriptionInfo GetInputDescriptionByID(string inputID)
        {

            SystemLogger.WriteInformation("Calling  GetOutputDescriptionByID");

            InputDescriptionInfo inpDescription = null;
            inpDescription = new InputDescriptionInfo();
            inpDescription.version = "1.0";
            inpDescription.Name = "InputDescriptionName";
            inpDescription.Description = "InputDescription";
            return inpDescription;
        }
        private ResponseStatus PutInputMaskStateByID(string inputID, string state)
        {

            SystemLogger.WriteInformation("Calling  PutInputMaskStateByID");

            try
            {

                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus PutInputDescriptionbyID(string inputID, InputDescriptionInfo inputDescription)
        {

            SystemLogger.WriteInformation("Calling  PutInputDescriptionbyID");

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus PutInputsListMaskState(string state)
        {

            SystemLogger.WriteInformation("Calling  PutInputMaskStateByID");

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private static InputStatus GetInputStatusDetails()
        {
            InputStatus inputStatus = new InputStatus();
            inputStatus.ID = 1;
            inputStatus.UID = "{" + Guid.NewGuid().ToString() + "}";
            inputStatus.Mask = MaskState.Monitored;
            inputStatus.MaskSpecified = true;
            inputStatus.version = "1.0";
            inputStatus.Connection = ConnectionState.OK;
            inputStatus.Input = InputState.Active;
            inputStatus.InputSpecified = true;
            inputStatus.ConnectionSpecified = true;
            return inputStatus;
        }
        #endregion
        #region CardHolders
        private ResponseStatus CreateCardHolderObject(CredentialHolderInfo holder)
        {

            if (holder == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);
            SystemLogger.WriteInformation(string.Format("Creating CredentialHolderInfo : \r\n{0}", holder.ToXml()));

            try
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.DeviceError, "This is testing response");
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }


        }
        private ResponseStatus UpdateCardHolderObject(CredentialHolderInfo holder)
        {

            if (holder == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);



            SystemLogger.WriteInformation(string.Format("Updating CredentialHolderInfo : \r\n{0}", holder.ToXml()));

            try
            {



                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }

        }
        private void AssignRole(ReferenceID roleID)
        {

            Guid guid = Guid.NewGuid();
            Guid.TryParse(roleID.GUID, out guid);

        }
        private ResponseStatus DeleteCardHolderObject(string cardholderlUID)
        {

            if (string.IsNullOrEmpty(cardholderlUID)) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);



            SystemLogger.WriteInformation(string.Format("Deleting  CredentialHolderInfo by GUID {0}", cardholderlUID));

            try
            {

                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus PutCardHolderObjectState(string credentialHolderUID, string activeState)
        {
            if (string.IsNullOrEmpty(credentialHolderUID)) Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, "credentialHolderUID");



            SystemLogger.WriteInformation(string.Format("Calling  PutCardHolderObjectState by GUID {0}", credentialHolderUID));


            try
            {



                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private CredentialHolderInfo GetCredentialHolderObject(string cardholderlUID)
        {

            if (string.IsNullOrEmpty(cardholderlUID)) return null;



            CredentialHolderInfo holderInfo = new CredentialHolderInfo();
            holderInfo.Name = "FirstName" + "," + "LastName";
            holderInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            holderInfo.GivenName = "FirstName";
            holderInfo.Surname = "LastName";
            holderInfo.State = StateOfCredentialHolder.Active;
            holderInfo.Description = "This is " + holderInfo.Name;
            holderInfo.ActiveTill = DateTime.Now.AddYears(5);
            holderInfo.Disability = false;
            holderInfo.version = "1.0";
            holderInfo.RoleIDList = new ReferenceID[1];
            holderInfo.RoleIDList[0] = new ReferenceID();
            holderInfo.RoleIDList[0].GUID = "{" + Guid.NewGuid().ToString() + "}";


            return holderInfo;
        }
        private CredentialHolderState GetCredentialHolderObjectState(string credentialHolderUID)
        {
            if (string.IsNullOrEmpty(credentialHolderUID)) return null;



            SystemLogger.WriteInformation(string.Format("Calling  GetCardHolderObjectState by GUID {0}", credentialHolderUID));
            CredentialHolderState cardHolderState = new CredentialHolderState();
            cardHolderState.version = "1.0";
            cardHolderState.State = StateOfCredentialHolder.Active;
            return cardHolderState;
        }
        #endregion
        #region Credentials
        private ResponseStatus CreateCredentialObject(CredentialInfo credentialInfo)
        {
            if (credentialInfo == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);
            SystemLogger.WriteInformation(string.Format("Saving credentialInfo : \r\n{0}", credentialInfo.ToXml()));

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus DeleteCredentialById(string credentialUID)
        {

            if (string.IsNullOrEmpty(credentialUID)) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);
            SystemLogger.WriteInformation(string.Format("Deleting  CredentialObject by GUID {0}", credentialUID));

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private CredentialInfoList GetCredentialsListInfo(string credentialHolderID, string identifierValue)
        {
            CredentialInfoList credntialList = null;
            credntialList = new CredentialInfoList();
            credntialList.version = "1.0";
            credntialList.CredentialInfo = new CredentialInfo[1];



            credntialList.CredentialInfo[0] = new CredentialInfo();

            credntialList.CredentialInfo[0].CredentialInfoExtension = new AreaControlExtension[1];
            credntialList.CredentialInfo[0].CredentialInfoExtension[0] = new AreaControlExtension();
            //CustomExtensionName - For interoperability, name must be registered with PSIA + XSD provided for the any-obj

            credntialList.CredentialInfo[0].CredentialInfoExtension[0].CustomExtensionName = "PSIA Credential extending property";
            credntialList.CredentialInfo[0].CredentialInfoExtension[0].Any = new XmlElement[1];
            credntialList.CredentialInfo[0].CredentialInfoExtension[0].Any[0] = SerializeToXmlElement(new CredentialInfoExtension() { Name = "CredentialInfoExtension", version = "1.0" });


            credntialList.CredentialInfo[0].ID = 1;
            credntialList.CredentialInfo[0].UID = "{" + Guid.NewGuid().ToString() + "}";
            credntialList.CredentialInfo[0].AssignedToID = new ReferenceID() { ID = 1, GUID = "{" + Guid.NewGuid().ToString() + "}" };

            credntialList.CredentialInfo[0].Name = "CredentialName";
            credntialList.CredentialInfo[0].Description = "CredentialSescription";
            credntialList.CredentialInfo[0].version = "1.0";
            credntialList.CredentialInfo[0].Disability = false;
            credntialList.CredentialInfo[0].DisabilitySpecified = true;
            credntialList.CredentialInfo[0].ValidFrom = DateTime.Now;
            credntialList.CredentialInfo[0].ValidFromSpecified = true;
            credntialList.CredentialInfo[0].ValidTo = DateTime.Now.AddYears(5);
            credntialList.CredentialInfo[0].ValidToSpecified = true;
            credntialList.CredentialInfo[0].State = StateOfCredential.Active;


            credntialList.CredentialInfo[0].IdentifierInfoList = new IdentifierInfo[1];
            credntialList.CredentialInfo[0].IdentifierInfoList[0] = new IdentifierInfo();
            credntialList.CredentialInfo[0].IdentifierInfoList[0].Type = IdentifierType.Card;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].ValueEncoding = IdentifierInfoValueEncoding.String;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].Value = "12345678";

            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList = new IdentifierCardComponent[2];
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[0] = new IdentifierCardComponent();
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[0].Type = CardFormatDataFieldType.FacilityCode;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[0].ValueEncoding = IdentifierInfoValueEncoding.Decimal;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[0].Value = "1";
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[1] = new IdentifierCardComponent();
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[1].Type = CardFormatDataFieldType.IssueCode;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[1].ValueEncoding = IdentifierInfoValueEncoding.Decimal;
            credntialList.CredentialInfo[0].IdentifierInfoList[0].CardComponentList[1].Value = "2";
            credntialList.CredentialInfo[0].IdentifierInfoList[0].Format = new ReferenceID() { ID = 2, GUID = "{" + Guid.NewGuid().ToString() + "}", IDSpecified = true, Name = "FormatName" };



            return credntialList;
        }
        private CredentialInfo GetCredentialInfo(string credentialUID)
        {
            CredentialInfo credntialInfo = null;
            SystemLogger.WriteInformation("Calling  GetCredentialInfo");


            credntialInfo = new CredentialInfo();
            credntialInfo.ID = 1;
            credntialInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            credntialInfo.AssignedToID = new ReferenceID() { ID = 1, GUID = "{" + Guid.NewGuid().ToString() + "}" };

            credntialInfo.Name = "CredentialName";
            credntialInfo.Description = "CredentialSescription";
            credntialInfo.version = "1.0";
            credntialInfo.Disability = false;
            credntialInfo.DisabilitySpecified = true;
            credntialInfo.ValidFrom = DateTime.Now;
            credntialInfo.ValidFromSpecified = true;
            credntialInfo.ValidTo = DateTime.Now.AddYears(5);
            credntialInfo.ValidToSpecified = true;
            credntialInfo.State = StateOfCredential.Active;


            credntialInfo.IdentifierInfoList = new IdentifierInfo[1];
            credntialInfo.IdentifierInfoList[0] = new IdentifierInfo();

            credntialInfo.CredentialInfoExtension = new AreaControlExtension[1];
            credntialInfo.CredentialInfoExtension[0] = new AreaControlExtension();
            credntialInfo.CredentialInfoExtension[0].CustomExtensionName = "PSIA Credential extending property";
            credntialInfo.CredentialInfoExtension[0].Any = new XmlElement[1];
            credntialInfo.CredentialInfoExtension[0].Any[0] = SerializeToXmlElement(new CredentialInfoExtension() { Name = "CredentialInfoExtension", version = "1.0" });


            credntialInfo.IdentifierInfoList[0].Type = IdentifierType.Card;
            credntialInfo.IdentifierInfoList[0].ValueEncoding = IdentifierInfoValueEncoding.String;
            credntialInfo.IdentifierInfoList[0].Value = "12345678";

            credntialInfo.IdentifierInfoList[0].CardComponentList = new IdentifierCardComponent[2];
            credntialInfo.IdentifierInfoList[0].CardComponentList[0] = new IdentifierCardComponent();
            credntialInfo.IdentifierInfoList[0].CardComponentList[0].Type = CardFormatDataFieldType.FacilityCode;
            credntialInfo.IdentifierInfoList[0].CardComponentList[0].ValueEncoding = IdentifierInfoValueEncoding.Decimal;
            credntialInfo.IdentifierInfoList[0].CardComponentList[0].Value = "1";
            credntialInfo.IdentifierInfoList[0].CardComponentList[1] = new IdentifierCardComponent();
            credntialInfo.IdentifierInfoList[0].CardComponentList[1].Type = CardFormatDataFieldType.IssueCode;
            credntialInfo.IdentifierInfoList[0].CardComponentList[1].ValueEncoding = IdentifierInfoValueEncoding.Decimal;
            credntialInfo.IdentifierInfoList[0].CardComponentList[1].Value = "2";
            credntialInfo.IdentifierInfoList[0].Format = new ReferenceID() { ID = 2, GUID = "{" + Guid.NewGuid().ToString() + "}", IDSpecified = true, Name = "FormatName" };
            return credntialInfo;
        }
        private ResponseStatus PutCredentialObjectState(string credentialUID, string activeState)
        {
            if (string.IsNullOrEmpty(credentialUID)) Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, "credentialUID");
            SystemLogger.WriteInformation(string.Format("Calling  PutCredentialObjectState by GUID {0}", credentialUID));

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private CredentialState GetCredentialStateById(string credentialUID)
        {
            if (string.IsNullOrEmpty(credentialUID)) return null;
            SystemLogger.WriteInformation(string.Format("Calling  GetCredentialObjectByGUID by GUID {0}", credentialUID));


            CredentialState credentialState = new CredentialState();
            credentialState.State = StateOfCredential.Active;
            credentialState.version = "1.0";
            return credentialState;
        }
        #endregion
        #region Partitions
        private PartitionInfoList GetPartitionsInfoList()
        {
            PartitionInfoList partitionsInfo = null;
            partitionsInfo = new PartitionInfoList();
            partitionsInfo.version = "1.0";


            partitionsInfo.PartitionInfo = new PartitionInfo[1];
            partitionsInfo.PartitionInfo[0] = GetPartitionInfoDetails();
            return partitionsInfo;

        }
        private PartitionInfo GetPartitionInfoById(string partitionID)
        {
            PartitionInfo partitionInfo = null;
            SystemLogger.WriteInformation("Calling  GetPartitionInfoAction");
            partitionInfo = GetPartitionInfoDetails();
            return partitionInfo;
        }
        private static PartitionInfo GetPartitionInfoDetails()
        {
            PartitionInfo partitionInfo = new PartitionInfo();
            partitionInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            partitionInfo.ID = 1;
            partitionInfo.Name = "PartitionName";
            partitionInfo.Description = "PartitionDescription";
            partitionInfo.version = "1.0";
            partitionInfo.AccessControl = new PartitionInfoForAccessControl();
            return partitionInfo;
        }
        #endregion
        #region Roles
        private ResponseStatus CreateRoleObject(RoleInfo roleInfoItem)
        {
            if (roleInfoItem == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Saving roleInfo : \r\n{0}", roleInfoItem.ToXml()));

            try
            {





                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus UpdateRole(RoleInfo roleInfoItem)
        {
            if (roleInfoItem == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);
            SystemLogger.WriteInformation(string.Format("Updating RoleInfo : \r\n{0}", roleInfoItem.ToXml()));

            try
            {

                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus DeleteRolelById(string roleUid)
        {
            if (string.IsNullOrEmpty(roleUid)) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Deleting  DeleteRolelById by GUID {0}", roleUid));

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private RoleInfo GetRoleInfo(string RoleId)
        {
            if (string.IsNullOrEmpty(RoleId)) return null;


            RoleInfo roleInfo = new RoleInfo();
            roleInfo.version = "1.0";
            roleInfo.Name = "RolName";
            roleInfo.Description = "RoleDescription";
            roleInfo.PermissionIDList = new PermissionIDList();
            roleInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            return roleInfo;
        }
        #endregion
        #region Portals
        private static PortalStatus GetPortalStatusDetails()
        {
            PortalStatus portaStatus = new PortalStatus();

            portaStatus.ID = 1;
            portaStatus.UID = "{" + Guid.NewGuid().ToString() + "}";

            portaStatus.Connection = ConnectionState.OK;

            portaStatus.ConnectionSpecified = true;
            portaStatus.Latch = LatchState.Unknown;
            portaStatus.LatchSpecified = true;

            portaStatus.PortalForcedMask = MaskState.Monitored;
            portaStatus.PortalForcedMaskSpecified = true;

            portaStatus.PortalHeldMask = MaskState.Monitored;
            portaStatus.PortalHeldMaskSpecified = true;

            portaStatus.PortalForced = PortalForcedState.Forced;
            portaStatus.PortalForcedSpecified = true;

            portaStatus.PortalHeld = PortalHeldState.Held;
            portaStatus.PortalHeldSpecified = true;
            portaStatus.PortalOpen = PortalOpenState.Open;
            portaStatus.PortalOpenSpecified = true;
            portaStatus.version = "1.0";
            return portaStatus;
        }
        #endregion
        #region CardFormats
        private ResponseStatus CreateMagstripeCardFormatObject(MagstripeCardFormatInfo magstripeCardFormatInfo)
        {
            if (magstripeCardFormatInfo == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Calling CreateMagstripeCardFormatObject for data: \r\n{0}", magstripeCardFormatInfo.ToXml()));

            try
            {




                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus UpdateMagstripeCardFormatObject(MagstripeCardFormatInfo magstripeCardFormatInfo)
        {

            if (magstripeCardFormatInfo == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Calling UpdateMagstripeCardFormatObject for data: \r\n{0}", magstripeCardFormatInfo.ToXml()));

            try
            {




                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus CreateWiegandCardFormatObject(WiegandCardFormatInfo wiegandcardFormatInfo)
        {
            if (wiegandcardFormatInfo == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Calling CreateWiegandCardFormatObject for data: \r\n{0}", wiegandcardFormatInfo.ToXml()));

            try
            {




                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private ResponseStatus UpdateWiegandCardFormatObject(WiegandCardFormatInfo wiegandcardFormatInfo)
        {
            if (wiegandcardFormatInfo == null) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);

            SystemLogger.WriteInformation(string.Format("Calling UpdateWiegandCardFormatObject for data: \r\n{0}", wiegandcardFormatInfo.ToXml()));

            try
            {




                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        private CardFormatInfoList GetCardFormatListInfo()
        {
            CardFormatInfoList cardFormats = null;
            cardFormats = new CardFormatInfoList();
            cardFormats.version = "1.0";
            cardFormats.WiegandCardFormatInfo = new WiegandCardFormatInfo[1];
            cardFormats.WiegandCardFormatInfo[0] = new WiegandCardFormatInfo();

            cardFormats.WiegandCardFormatInfo[0].WiegandCardFormatInfoExtension = new AreaControlExtension[1];
            cardFormats.WiegandCardFormatInfo[0].WiegandCardFormatInfoExtension[0] = new AreaControlExtension();
            cardFormats.WiegandCardFormatInfo[0].WiegandCardFormatInfoExtension[0].CustomExtensionName = "PSIA Card Format extending property";
            cardFormats.WiegandCardFormatInfo[0].WiegandCardFormatInfoExtension[0].Any = new XmlElement[1];
            cardFormats.WiegandCardFormatInfo[0].WiegandCardFormatInfoExtension[0].Any[0] = SerializeToXmlElement(new CardFormatInfoExtention() { Name = "WiegandCardFormatInfoExtension", version = "1.0" });


            cardFormats.WiegandCardFormatInfo[0].ID = 1;
            cardFormats.WiegandCardFormatInfo[0].UID = "{" + Guid.NewGuid().ToString() + "}";
            cardFormats.WiegandCardFormatInfo[0].Name = "CardFormatName";
            cardFormats.WiegandCardFormatInfo[0].Description = "CardFormatDescription";
            cardFormats.WiegandCardFormatInfo[0].Length = 26;
            cardFormats.WiegandCardFormatInfo[0].version = "1.0";
            cardFormats.WiegandCardFormatInfo[0].Item = new WiegandCardFormatDataFieldList();
            ((WiegandCardFormatDataFieldList)cardFormats.WiegandCardFormatInfo[0].Item).DataField = new WiegandCardFormatDataField[1];
            var fieldItem = ((WiegandCardFormatDataFieldList)cardFormats.WiegandCardFormatInfo[0].Item);
            fieldItem.DataField[0] = new WiegandCardFormatDataField();
            fieldItem.DataField[0].Name = "FieldName";
            fieldItem.DataField[0].Type = CardFormatDataFieldType.CardNum;
            fieldItem.DataField[0].Offset = 1;
            fieldItem.DataField[0].Length = 10;

            return cardFormats;
        }
        private MagstripeCardFormatInfo GetMagstripeCardFormatInfo(string cardFormatUID)
        {
            MagstripeCardFormatInfo magstripeCardFormatInfo = null;
            SystemLogger.WriteInformation(string.Format("Calling GetMagstripeCardFormatInfo for data: \r\n{0}", cardFormatUID));


            magstripeCardFormatInfo = new MagstripeCardFormatInfo();
            magstripeCardFormatInfo.MagstripeCardFormatInfoExtension = new AreaControlExtension[1];
            magstripeCardFormatInfo.MagstripeCardFormatInfoExtension[0] = new AreaControlExtension();
            magstripeCardFormatInfo.MagstripeCardFormatInfoExtension[0].CustomExtensionName = "PSIA Card Format extending property";
            magstripeCardFormatInfo.MagstripeCardFormatInfoExtension[0].Any = new XmlElement[1];
            magstripeCardFormatInfo.MagstripeCardFormatInfoExtension[0].Any[0] = SerializeToXmlElement(new CardFormatInfoExtention() { Name = "MagstripeCardFormatInfoExtension", version = "1.0" });


            magstripeCardFormatInfo.ID = 1;
            magstripeCardFormatInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            magstripeCardFormatInfo.Name = "CardFormatName";
            magstripeCardFormatInfo.Description = "CardFormatDescription";
            magstripeCardFormatInfo.Length = 26;
            magstripeCardFormatInfo.version = "1.0";
            magstripeCardFormatInfo.Item = new MagstripeCardFormatDataFieldList();
            ((MagstripeCardFormatDataFieldList)magstripeCardFormatInfo.Item).DataField = new MagstripeCardFormatDataField[1];
            var fieldItem = ((MagstripeCardFormatDataFieldList)magstripeCardFormatInfo.Item);
            fieldItem.DataField[0] = new MagstripeCardFormatDataField();
            fieldItem.DataField[0].Name = "FieldName";
            fieldItem.DataField[0].Type = CardFormatDataFieldType.CardNum;
            fieldItem.DataField[0].Offset = 1;
            fieldItem.DataField[0].Length = 10;
            return magstripeCardFormatInfo;
        }
        private WiegandCardFormatInfo GetWiegandCardFormatInfo(string cardFormatUID)
        {

            WiegandCardFormatInfo wiegandCardFormatInfo = null;
            SystemLogger.WriteInformation(string.Format("Calling GetWiegandCardFormatInfo for data: \r\n{0}", cardFormatUID));

            wiegandCardFormatInfo = new WiegandCardFormatInfo();


            wiegandCardFormatInfo.WiegandCardFormatInfoExtension = new AreaControlExtension[1];
            wiegandCardFormatInfo.WiegandCardFormatInfoExtension[0] = new AreaControlExtension();
            wiegandCardFormatInfo.WiegandCardFormatInfoExtension[0].CustomExtensionName = "PSIA Card Format extending property";
            wiegandCardFormatInfo.WiegandCardFormatInfoExtension[0].Any = new XmlElement[1];
            wiegandCardFormatInfo.WiegandCardFormatInfoExtension[0].Any[0] = SerializeToXmlElement(new CardFormatInfoExtention() { Name = "WiegandCardFormatInfoExtension", version = "1.0" });


            wiegandCardFormatInfo.ID = 1;
            wiegandCardFormatInfo.UID = "{" + Guid.NewGuid().ToString() + "}";
            wiegandCardFormatInfo.Name = "CardFormatName";
            wiegandCardFormatInfo.Description = "CardFormatDescription";
            wiegandCardFormatInfo.Length = 26;
            wiegandCardFormatInfo.version = "1.0";
            wiegandCardFormatInfo.Item = new WiegandCardFormatDataFieldList();
            ((WiegandCardFormatDataFieldList)wiegandCardFormatInfo.Item).DataField = new WiegandCardFormatDataField[1];
            var fieldItem = ((WiegandCardFormatDataFieldList)wiegandCardFormatInfo.Item);
            fieldItem.DataField[0] = new WiegandCardFormatDataField();
            fieldItem.DataField[0].Name = "FieldName";
            fieldItem.DataField[0].Type = CardFormatDataFieldType.CardNum;
            fieldItem.DataField[0].Offset = 1;
            fieldItem.DataField[0].Length = 10;
            return wiegandCardFormatInfo;
        }
        private ResponseStatus DeleteCardFormatById(string cardFormatUID)
        {
            if (string.IsNullOrEmpty(cardFormatUID)) return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation);



            SystemLogger.WriteInformation(string.Format("Deleting  DeleteCardFormatById by GUID {0}", cardFormatUID));

            try
            {


                return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Utilities.GetResponseStatus(1, ActionStatusCode.InvalidOperation, ex.Message);
            }
        }
        #endregion
        #region Miscellaneous
        private AreaControlStatus GetStatusAction()
        {
            AreaControlStatus status = new AreaControlStatus();
            status.PowerStatus = new PowerStatus();
            status.PowerStatus.BatteryPower = BatteryPowerState.OK;
            status.PowerStatus.BatteryPowerSpecified = true;
            status.CommunicationStatus = new CommunicationStatus();
            status.CommunicationStatus.Connection = ConnectionState.OK;
            status.CommunicationStatus.ConnectionSpecified = true;
            return status;

        }
        private AreaControlConfiguration GetConfugurationAction()
        {

            SystemLogger.WriteInformation("Calling  GetConfuguration");
            AreaControlConfiguration configuration = new AreaControlConfiguration();
            configuration.version = "1.0";
            configuration.CapabilityList = new Capability[2];
            configuration.CapabilityList[0] = Capability.AccessControl;
            configuration.CapabilityList[1] = Capability.Intrusion;
            configuration.ModelName = " 9000";
            configuration.Manufacturer = "Tyco";
            configuration.HardwareVersion = "1.0";
            configuration.FirmwareVersion = "1.0";

            return configuration;
        }
        private static RequiredIdentifierInfo GetRequiredIdentifier()
        {
            RequiredIdentifierInfo requiredIdentifierInfo = new RequiredIdentifierInfo();
            requiredIdentifierInfo.Item = new IdentifierList();
            requiredIdentifierInfo.Item.IdentifierType = new IdentifierType[1];
            requiredIdentifierInfo.Item.IdentifierType[0] = IdentifierType.Card;
            return requiredIdentifierInfo;
        }
        #endregion
        #region Permissions
        private PermissionInfoList GetPermissionsList()
        {
            PermissionInfoList permissions = new PermissionInfoList();
            permissions.version = "1.0";


            return permissions;
        }
        private ResponseStatus CreatePersmissionObject(PermissionInfo permissionInfo)
        {
            return Utilities.GetResponseStatus(1, ActionStatusCode.OK);
        }
        private PermissionInfo GetPermissionByID(string permissionID)
        {
            PermissionInfo permission = new PermissionInfo();
            permission.version = "1.0";
            permission.ID = 1;
            permission.UID = "{" + Guid.NewGuid().ToString() + "}";
            permission.Name = "Unknown";
            permission.Description = "Unknown";
            return permission;
        }
        #endregion
        private static void OnStreamAvailable(Stream stream, HttpContent headers, TransportContext context)
        {
            var streamWriter = new StreamWriter(stream);
            writer = streamWriter;
        }
        private static void TimerCallback(object state)
        {
            try
            {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                if (writer == null)
                {
                    return;
                }
                //sending events
                if (eventsCollection != null)
                {
                    //Check if you have an event to send pack to all the registered PACS if not send a metadata marker to keep stream alive
                    if (writer != null)
                    {                       
                        try
                        {
                            if (eventsCollection.Count == 0)
                            {
                                //sending marker if no events in pipeline
                                SendMarkerData();
                            }

                            GenerateCredentialMessage();

                            SystemLogger.WriteInformation("TimerCallback: PSIAController.eventsCollection.Count: " + eventsCollection.Count);
                            eventsCollection.ToList().ForEach(e =>
                            {
                                try
                                {
                                    if (e.Value != null)
                                    {
                                        if (e.Value.MetadataHeader != null && e.Value.MetadataHeader.MetaLink != null)
                                        {
                                            SystemLogger.WriteInformation("TimerCallback: Sending Attribute XML.");
                                            if (!failedEvents.ContainsKey(e.Value.MetadataHeader.MetaLink))
                                            {
                                                SystemLogger.WriteInformation("TimerCallback: XML not present in FailedEvents dictionary.");
                                                string boundaryConfig = ConfigurationSettings.AppSettings["BoundarySetting"];
                                                var streamStr = boundaryConfig;
                                                streamStr += e.Value.ToXml();

                                                if (!string.IsNullOrEmpty(streamStr))
                                                {
                                                    if (writer != null && writer.BaseStream.CanWrite)
                                                    {
                                                        SystemLogger.WriteInformation(string.Format("Sending event to PLAI agent: {0}", streamStr));
                                                        try
                                                        {
                                                            writer.Write(streamStr);
                                                            writer.Flush();
                                                            SystemLogger.WriteInformation(string.Format("(Not present in failed) Attestation sent at : {0} with MetaLink: {1}", DateTime.Now, e.Value.MetadataHeader.MetaLink));
                                                            Console.WriteLine(string.Format("(Not present in failed) Attestation sent at : {0} with MetaLink: {1}", DateTime.Now, e.Value.MetadataHeader.MetaLink));
                                                            AreaControlEvent outValue;
                                                            eventsCollection.TryRemove(e.Key, out outValue);
                                                            processingEvents.TryAdd(e.Value.MetadataHeader.MetaLink, DateTime.Now);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            SystemLogger.WriteError(ex.Message);
                                                        }


                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //If PLAI Agent failed to process the Attestation we will send it again according to the below conditions
                                                //1. For First two attempts it will be sent at an interval of 10s
                                                //2. After two attempts upto 10 attempts it will be sent at an interval of 60s
                                                //3. Post that if it still fails metalink UID will be changed in attestation XML.
                                                var failedEventDetails = failedEvents[e.Value.MetadataHeader.MetaLink];
                                                if ((failedEventDetails.FailedCount <= 1 && (DateTime.Now - failedEventDetails.FailedTimeStamp).TotalSeconds > FldReattemptDurationForFstTwo) ||
                                                (failedEventDetails.FailedCount < 10 && failedEventDetails.FailedCount > 1 && (DateTime.Now - failedEventDetails.FailedTimeStamp).TotalSeconds > FldReattemptDurationForAfterTwo))
                                                {
                                                    string boundaryConfig = ConfigurationSettings.AppSettings["BoundarySetting"];
                                                    var streamStr = boundaryConfig;
                                                    streamStr += e.Value.ToXml();

                                                    if (!string.IsNullOrEmpty(streamStr))
                                                    {
                                                        if (writer != null && writer.BaseStream.CanWrite)
                                                        {
                                                            SystemLogger.WriteInformation(string.Format("Sending event to PLAI agent: {0}", streamStr));
                                                            try
                                                            {
                                                                writer.Write(streamStr);
                                                                writer.Flush();
                                                                SystemLogger.WriteInformation(string.Format("(Present in failed) Attestation sent at : {0} with MetaLink: {1}", DateTime.Now, e.Value.MetadataHeader.MetaLink));
                                                                Console.WriteLine(string.Format("(Present in failed) Attestation sent at : {0} with MetaLink: {1}", DateTime.Now, e.Value.MetadataHeader.MetaLink));
                                                                AreaControlEvent outValue;
                                                                eventsCollection.TryRemove(e.Key, out outValue);
                                                                processingEvents.TryAdd(e.Value.MetadataHeader.MetaLink, DateTime.Now);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                SystemLogger.WriteError(ex.Message);
                                                            }


                                                        }
                                                    }
                                                }
                                                else if(failedEventDetails.FailedCount > 9 && (DateTime.Now - failedEventDetails.FailedTimeStamp).TotalSeconds > 60)
                                                {
                                                    SystemLogger.WriteInformation("Updated Metalink for " + e.Value.MetadataHeader.MetaLink);
                                                    Console.WriteLine("Updated Metalink for " + e.Value.MetadataHeader.MetaLink);
                                                    UpdateMetaLinkUID(e.Value.MetadataHeader.MetaLink);
                                                    FailedDetails val;
                                                    failedEvents.TryRemove(e.Value.MetadataHeader.MetaLink, out val);
                                                }
                                                else
                                                {
                                                    SendMarkerData();
                                                }
                                            }
                                        }
                                        else 
                                        {
                                            SystemLogger.WriteInformation("TimerCallback: Sending AccessAttestation XML");
                                            string boundaryConfig = ConfigurationSettings.AppSettings["BoundarySetting"];
                                            var streamStr = boundaryConfig;
                                            streamStr += e.Value.ToXml();

                                            if (!string.IsNullOrEmpty(streamStr))
                                            {
                                                if (writer != null && writer.BaseStream.CanWrite)
                                                {                                                    
                                                    try
                                                    {
                                                        writer.Write(streamStr);
                                                        writer.Flush();
                                                        SystemLogger.WriteInformation(string.Format("TimerCallback: Sending AccessAttestation or markee at : {0}", DateTime.Now));
                                                        Console.WriteLine(string.Format("TimerCallback: Sending AccessAttestation or markee at : {0}", DateTime.Now));
                                                        AreaControlEvent outValue;
                                                        eventsCollection.TryRemove(e.Key, out outValue);                                                        
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        SystemLogger.WriteError(ex.Message);
                                                    }
                                                }
                                            }
                                        }                                                                              
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AreaControlEvent outValue;
                                    eventsCollection.TryRemove(e.Key, out outValue);
                                    SystemLogger.WriteError(ex.ToString());
                                }

                            });
                            eventsCollection = new ConcurrentDictionary<string, AreaControlEvent>();                            

                            foreach (var item in processingEvents)
                            {
                                if ((DateTime.Now - item.Value).TotalSeconds > ackLostReattemptDuration)
                                {
                                    SystemLogger.WriteInformation("Updating processing dictionary.");                                    
                                    if (failedEvents.ContainsKey(item.Key))
                                    {
                                        failedEvents[item.Key].FailedCount++;
                                        failedEvents[item.Key].FailedTimeStamp = DateTime.Now;
                                        SystemLogger.WriteInformation("TimerCallback: Updated in failed with metalink: " + item.Key + " FailedDetails: " + failedEvents[item.Key].FailedCount + " | " + failedEvents[item.Key].FailedTimeStamp);
                                        Console.WriteLine("TimerCallback: Updated in failed with metalink: " + item.Key + " FailedDetails: " + failedEvents[item.Key].FailedCount + " | " + failedEvents[item.Key].FailedTimeStamp);
                                    }
                                    else
                                    {
                                        FailedDetails failedDetails = new FailedDetails
                                        {
                                            FailedCount = 0,
                                            FailedTimeStamp = DateTime.Now
                                        };
                                        failedEvents.TryAdd(item.Key, failedDetails);
                                        SystemLogger.WriteInformation("TimerCallback: Added in failed with metalink: " + item.Key + " FailedDetails: " + failedDetails.FailedCount + " | " + failedDetails.FailedTimeStamp);
                                        Console.WriteLine("TimerCallback: Added in failed with metalink: " + item.Key + " FailedDetails: " + failedDetails.FailedCount + " | " + failedDetails.FailedTimeStamp);
                                    }
                                    DateTime value;
                                    processingEvents.TryRemove(item.Key, out value);
                                }
                            }                            
                        }
                        catch (Exception ex)
                        {
                            SystemLogger.WriteError(ex.ToString());
                        }
                    }
                }


            }
            finally
            {
                timer.Change(TimerFreqMSec, TimerFreqMSec);

            }
        }
        private static void SendMarkerData()
        {
            var curDateTime = DateTime.Now;
            var marker = new AreaControlEvent();
            marker.version = "1.0";
            marker.MetadataHeader = new metaHeader();
            marker.MetadataHeader.MetaVersion = 1;
            marker.MetadataHeader.MetaID = string.Format("/psialliance.org/Metadata/marker/{0}", curDateTime);
            marker.MetadataHeader.MetaSourceID = SourceID;
            marker.MetadataHeader.MetaSourceLocalID = 0;
            marker.MetadataHeader.MetaTime = curDateTime;
            marker.MetadataHeader.MetaPriority = 4;

            if (writer != null && writer.BaseStream.CanWrite)
            {

                try
                {
                    if (writer != null && writer.BaseStream.CanWrite && !ClearMarkerForNoData)
                    {
                        var streamStr = !ClearMarkerForNoData ? marker.ToXml() : string.Empty;
                        if (!string.IsNullOrEmpty(streamStr))
                        {

                            writer.Write(streamStr);
                            writer.Flush();
                            SystemLogger.WriteInformation(string.Format("Markee sent at : {0}", DateTime.Now));
                            Console.WriteLine(string.Format("Markee sent at : {0}", DateTime.Now));

                        }
                    }
                }
                catch (Exception ex)
                {
                    SystemLogger.WriteError(ex.Message);
                }
            }
        }

        private static void UpdateMetaLinkUID(string metaLinkUID)
        {
            Guid guid = Guid.NewGuid();
            AreaControlEvent eventObject = new AreaControlEvent();
            string filePathInConfig = ConfigurationSettings.AppSettings["CovidAttestationXml"];
            string importPath = Path.GetFullPath(filePathInConfig);
            ReadAllFilesToFindStartingFromDirectory(importPath, metaLinkUID);
        }

        private static void ReadAllFilesToFindStartingFromDirectory(string topLevelDirectory, string metaLinkUID)
        {
            const string searchPattern = "*.xml";
            var subDirectories = Directory.EnumerateDirectories(topLevelDirectory);
            var filesInDirectory = Directory.EnumerateFiles(topLevelDirectory, searchPattern);

            foreach (var subDirectory in subDirectories)
            {
                ReadAllFilesToFindStartingFromDirectory(subDirectory, metaLinkUID);//recursion
            }

            IterateFilesToFindXml(filesInDirectory, topLevelDirectory, metaLinkUID);
        }

        private static void IterateFilesToFindXml(IEnumerable<string> files, string directory, string metaLinkUID)
        {
            AreaControlEvent eventObject = new AreaControlEvent();
            foreach (var file in files)
            {
                //Console.WriteLine("{0}", Path.Combine(directory, file));//for verification
                try
                {
                    if (File.Exists(file))
                    {
                        string inputXmlData = DeserealizeFile(file);
                        eventObject = Utilities.DeserializeObject<AreaControlEvent>(inputXmlData);
                        if (eventObject.MetadataHeader.MetaLink == metaLinkUID)
                        {                            
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(file);
                            xmlDoc.GetElementsByTagName("MetaLink")[0].InnerText = "{" + Guid.NewGuid().ToString() + "}";
                            xmlDoc.Save(file);
                            break;
                        }
                    }                    
                }
                catch (IOException ex)
                {
                    SystemLogger.WriteError("IterateFilesToFindXml: Exception: " + ex);
                }
            }
        }

        private static void GenerateCredentialMessage()
        {
            Guid guid = Guid.NewGuid();
            AreaControlEvent eventObject = new AreaControlEvent();
            string filePathInConfig = ConfigurationSettings.AppSettings["CovidAttestationXml"];
            string importPath = Path.GetFullPath(filePathInConfig);
            ReadAllFilesStartingFromDirectory(importPath);
        }

        private static void ReadAllFilesStartingFromDirectory(string topLevelDirectory)
        {
            const string searchPattern = "*.xml";
            var subDirectories = Directory.EnumerateDirectories(topLevelDirectory);
            var filesInDirectory = Directory.EnumerateFiles(topLevelDirectory, searchPattern);

            foreach (var subDirectory in subDirectories)
            {
                ReadAllFilesStartingFromDirectory(subDirectory);//recursion
            }

            IterateFiles(filesInDirectory, topLevelDirectory);
        }

        private static void IterateFiles(IEnumerable<string> files, string directory)
        {
            AreaControlEvent eventObject = new AreaControlEvent();
            foreach (var file in files)
            {
                Console.WriteLine("{0}", Path.Combine(directory, file));//for verification
                try
                {
                    if (File.Exists(file))
                    {
                        string inputXmlData = DeserealizeFile(file);
                        eventObject = Utilities.DeserializeObject<AreaControlEvent>(inputXmlData);
                    }
                    if (eventsCollection == null) eventsCollection = new ConcurrentDictionary<string, AreaControlEvent>();
                    if (eventsCollection.Count >= 0 /*&& !processingEvents.ContainsKey(eventObject.MetadataHeader?.MetaLink??string.Empty)*/)
                        eventsCollection.AddOrUpdate(Guid.NewGuid().ToString(), eventObject, (key, data) => data);
                }
                catch (IOException ex)
                {
                    SystemLogger.WriteError("IterateFiles: Exception: " + ex);                                    
                }
            }
        }




        private static string DeserealizeFile(string inputPath)
        {
            string xmlString = string.Empty;
            try
            {
                using (var streamReader = new StreamReader(new FileStream(inputPath, FileMode.Open, FileAccess.Read)))
                {
                    xmlString = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                SystemLogger.WriteError("DeserealizeFile: Exception: " + ex);
            }
            return xmlString;
        }
        public static IPEndPoint GetIPEndPointFromHostName(string hostName, int port)
        {
            var dns = Dns.GetHostAddresses(hostName);
            if (dns != null)
            {
                var addresses = dns.Where(ip => { return ValidateIPv4(ip.ToString()); });
                if (addresses != null)
                {
                    return new IPEndPoint(addresses.FirstOrDefault(), port);
                }
            }
            return null;
        }
        public static bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }
        //This method is obselete. Would be removed.
        public static string GetIssuerSignatureFromHeader()
        {
            return WebOperationContext.Current.IncomingRequest.Headers[ServiceConstants.IssuerSignatureHeader];
        }
        /// <summary>
        /// This is obselete. Would throw exception.
        /// </summary>
        /// <returns></returns>
        public static string[] GetCredentialsFromHeaders(HttpRequestHeaders headers)
        {

            var credentials = headers.FirstOrDefault(h => h.Key == "Authorization");
            if (credentials.Value == null) return null;

            string credential = null;


            credential = credentials.Value.FirstOrDefault().Trim();


            if (!String.IsNullOrEmpty(credential))
            {
                try
                {
                    string[] credentialParts = credential.Split(new[] { ' ' });
                    if (credentialParts.Length == 2 && credentialParts[0].Equals("basic", StringComparison.OrdinalIgnoreCase))
                    {
                        credential = Encoding.ASCII.GetString(Convert.FromBase64String(credentialParts[1]));
                        credentialParts = credential.Split(new[] { ':' });
                        if (credentialParts.Length == 2)
                            return credentialParts;
                    }
                }
                catch (Exception ex)
                {
                    SystemLogger.WriteError("GetCredentialsFromHeaders: Exception: " + ex);
                }
            }

            return null;
        }


        public static XmlElement SerializeToXmlElement(object o)
        {
            XmlDocument doc = new XmlDocument();

            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                new XmlSerializer(o.GetType()).Serialize(writer, o);
            }

            return doc.DocumentElement;
        }
        #endregion
        public class TestingStart
        {
            public System.DateTime TestDate { get; set; }
        }
    }
}
