﻿using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Tracing;

namespace Service
{
public class ErrorLogTraceWriter : ITraceWriter
    {
        public void Trace(HttpRequestMessage request, string category,

                           TraceLevel level, Action<TraceRecord> traceAction)

        {
            if(request!=null)
            {
                var data = request.Content.ReadAsStringAsync().Result;
                SystemLogger.WriteInformation(string.Format("Request content: {0}", data));
            }

            TraceRecord traceRecord = new TraceRecord(request, category, level);
            traceAction(traceRecord);
            ShowTrace(traceRecord);

        }



        private void ShowTrace(TraceRecord traceRecord)

        {

            SystemLogger.WriteInformation(

               String.Format(

                    "{0} {1}: Category={2}, Level={3} {4} {5} {6} {7}",

                    traceRecord.Request!=null ? traceRecord.Request.Method.ToString():"Unknown",
                    traceRecord.Request != null ? traceRecord.Request.RequestUri.ToString(): "Unknown",
                    traceRecord.Category,
                    traceRecord.Level,
                    traceRecord.Kind,
                    traceRecord.Operator,
                    traceRecord.Operation,
                    traceRecord.Exception != null
                       ? traceRecord.Exception.GetBaseException().Message
                        : !string.IsNullOrEmpty(traceRecord.Message)
                            ? traceRecord.Message
                            : string.Empty

                ));
        }

    }
}
