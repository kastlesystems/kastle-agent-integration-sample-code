﻿namespace Service
{
    using System;

    public interface IRequest<T, K>
    {
        int ResponseTimeOut { get; set; }
        int RequestTimeOut { get; set; }
        int ResponseRetriveRetries { get; set; }
        int RequestSendRetries { get; set; }
        event EventHandler<System.EventArgs> ProcessRequestCompleted;
        bool IsProcessRequestCompleted { get; }
        K PostRequest(T message, PostMethodType posMethod);
        void PostRequestAsync(T message, PostMethodType posMethod);

    }
}
