﻿namespace Service
{

    using System;
    using System.IO;
    /// <summary>
    /// Base class for streaming services. Provides basic functionality and configures service.
    /// </summary>
    /// <typeparam name="TService"></typeparam>
    public abstract class StreamService<TService> : IService<TService>
        where TService : Stream
       
    {
        private bool disposed;
        public TService Service { get; private set; }
        public bool IsDisposed { get { return disposed; }}
        
        /// <summary>
        /// Service configuration.
        /// </summary>
        public class ServiceConfiguration
        {
            public int ReadTimeOut { get; set; }
            public int WriteTimeOut { get; set; }
        }

     
        /// <summary>
        /// Creates base class object.
        /// </summary>
        /// <param name="Service">Service as stream</param>
        /// <param name="configure">Service configuration</param>
        public StreamService(TService Service,Action<ServiceConfiguration> configure=null)
        {
            if (Service == null)
                throw new ArgumentNullException("Service");

            var config = new ServiceConfiguration();
            this.Service = Service;
            configure(config);
            this.Service.ReadTimeout = config.ReadTimeOut;
            this.Service.WriteTimeout = config.WriteTimeOut;
           
        }
       
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.Service != null)
                    {
                      this.Service.Flush();
                      this.Service.Close();
                      this.Service = null;
                    }
                }
                this.disposed = true;
            }
        }

        ~StreamService()
        {
            Dispose(true);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
     
    }
    
}
