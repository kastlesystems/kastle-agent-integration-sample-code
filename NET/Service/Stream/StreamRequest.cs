﻿namespace Service
{
    using System;
    using System.IO;
    using System.Threading;



    /// <summary>
    /// Provides common functionality related with streamed read/write operations.
    /// </summary>
    /// <typeparam name="TInput">Request input data model</typeparam>
    /// <typeparam name="TOutput">Request output data model</typeparam>
    /// <typeparam name="TStream">Stream type (e.g. SslStream, NetworkStream, FileStream etc)</typeparam>
    public abstract class StreamRequest<TInput, TOutput,TStream> : IRequest<TInput, TOutput>
        where TStream : Stream
    {
        private readonly StreamService<TStream> serviceFactory;

        /// <summary>
        /// Gets stream service
        /// </summary>
        public  TStream Service { get; private set; }
        /// <summary>
        /// Gets or sets value that specifies wait response timeout (milliseconds).
        /// </summary>
        public int ResponseTimeOut { get; set; }
        /// <summary>
        /// Gets or sets value that specifies wait request timeout (milliseconds).
        /// </summary>
        public int RequestTimeOut { get; set; }
        /// <summary>
        ///Gets or sets value that specifies number of fetching response attempts.
        /// </summary>
        public int ResponseRetriveRetries { get; set; }
        /// <summary>
        /// Gets or sets value that specifies number of send request attempts.
        /// </summary>
        public int RequestSendRetries { get; set; }
        /// <summary>
        /// Gets value indicating wheter is request process complited.
        /// </summary>
        public bool IsProcessRequestCompleted { get; private set; }
        /// <summary>
        /// Occurs when the request process is complited.
        /// </summary>
        public event EventHandler<EventArgs> ProcessRequestCompleted;

        /// <summary>
        /// Constructs base class object
        /// </summary>
        /// <param name="serviceFactort">Stream service object.</param>
        public StreamRequest(StreamService<TStream> serviceFactort)
        {
            if(serviceFactort==null)
                throw new ArgumentNullException("serviceFactort");

            this.serviceFactory = serviceFactort;
            this.Service = serviceFactort.Service;
            this.ResponseTimeOut = 60000; //default - 1 min
            this.RequestTimeOut = 60000; //default - 1 min
            this.ResponseRetriveRetries = 1;
            this.RequestSendRetries = 1;
        }
  
        public abstract TOutput PostRequest(TInput request, PostMethodType posMethod);
        public abstract void PostRequestAsync(TInput message, PostMethodType posMethod);

        protected virtual void RaiseProcessRequestCompleted(TOutput response)
        {
   
            if (ProcessRequestCompleted != null)
            {
                ProcessRequestCompleted(this, new EventArgs<TOutput>(response));
                this.IsProcessRequestCompleted = true;
            }
        }
        protected virtual byte[] ReadStream()
        {
            byte[] Data = new byte[2048];
            int cData = -1;
            Exception intException=null;
            bool IsFinished = false;

            IsFinished = TimeOutHelper.ExecuteMethod(() =>
            {
                try
                {
                    do
                    {
                        lock (serviceFactory)
                        {
                            if (serviceFactory != null && !serviceFactory.IsDisposed)
                            {
                                var service = serviceFactory.Service;

                                    if (service != null)
                                    {
                                        cData = service.Read(Data, 0, 2048);
                                    }
                                    Thread.Sleep(100);
                                }
                            
                        }
                    } while (cData <= 0);
                }
                catch(Exception ex)
                {
                    if (!IsFinished)
                     {
                         lock (this) 
                         {
                         intException = ex;
                         Data = null;
                         }
                        
                    }
                }
            }, this.ResponseTimeOut, this.ResponseRetriveRetries);

            if (intException != null) throw intException;
            return Data;
        }
        protected virtual void WriteStream(byte[] buffer, int offset, int count, bool flushData=true)
        {
            Exception innerException = null;
            bool isSuccessed = false;
            for (int i = 0; i < this.RequestSendRetries; i++)
            {
                try
                {
                    Service.Write(buffer, offset, count);
                    if (flushData)
                        Service.Flush();
                    isSuccessed = true;
                    break;
                }
                catch(Exception ex)
                {
                    innerException = ex;
                    Thread.Sleep(RequestTimeOut);
                }
            }

            if (!isSuccessed && innerException!=null)
                throw  innerException;
        }
    }
}
