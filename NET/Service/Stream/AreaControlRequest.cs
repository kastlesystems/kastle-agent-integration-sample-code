﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace Service
{
    public class AreaControlRequest : StreamRequest<byte[], byte[], NetworkStream>
    {


        public AreaControlRequest(StreamService<NetworkStream> streamService):base(streamService)
        {

        }

        public override byte[] PostRequest(byte[] request, PostMethodType posMethod)
        {
            this.WriteStream(request, 0, request.Length, true);
            return null;

        }

        public override void PostRequestAsync(byte[] message, PostMethodType posMethod)
        {
            Task.Factory.StartNew(() => this.WriteStream(message, 0, message.Length, true)); ;
        }
    }
}
