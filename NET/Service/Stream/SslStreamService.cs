﻿namespace Service
{
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;

    /// <summary>
    /// Provides ssl client streaming features. Service is 'disposable' and caller don't need to take care about releasing local resources. 
    /// Service can be used by StreamRequest class
    /// </summary>
    public class SslStreamService : StreamService<SslStream>  
    {
        private TcpClient client;

        private SslStreamService(TcpClient client,
                                 string serverName,
                                 int timeOutInterval,
                                 X509Certificate2 clientCertyificate = null): base(new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificateCallback)),
                                                                                   c => { c.WriteTimeOut = timeOutInterval; c.ReadTimeOut = Timeout.Infinite; })
        {
            if (client == null)
                throw new ArgumentNullException("client");

            if (string.IsNullOrEmpty(serverName))
                throw new ArgumentNullException("serverName");

            this.client = client;
        }

        /// <summary>
        /// Creates service.
        /// </summary>
        /// <param name="IPendPoint">Combination of IP address and TCP port.</param>
        /// <param name="serverName">Remote host full domain name.</param>
        /// <param name="timeOutInterval">Stream read/write operations timeout.</param>
        /// <param name="clientCertyificate">X509 client certificate.</param>
        /// <returns></returns>
        public static SslStreamService CreateService(IPEndPoint IPendPoint,
                                                     string serverName,
                                                     int timeOutInterval,
                                                     X509Certificate2 clientCertyificate=null)
        {
            TcpClient client = null;

            if (IPendPoint == null)
                throw new ArgumentNullException("IPendPoint");

            if (string.IsNullOrEmpty(serverName))
                throw new ArgumentNullException("serverName");

            

            client = new TcpClient(IPendPoint.AddressFamily);
            client.Connect(IPendPoint);

             var sslService = new SslStreamService(client,serverName,timeOutInterval, clientCertyificate);
             AuthenticateAsClient(sslService, clientCertyificate, serverName);
             return sslService;
        }

        /// <summary>
        /// Creates service.
        /// </summary>
        /// <param name="socket">socket</param>
        /// <param name="onwSocket">If ownSocket parameter is false dosen't dispose socket.</param>
        /// <param name="serverName">Remote host full domain name.</param>
        /// <param name="timeOutInterval">Stream read/write operations timeout.</param>
        /// <param name="clientCertyificate">X509 client certificate.</param>
        /// <returns></returns>
        public static SslStreamService CreateService(Socket socket,
                                                          bool onwSocket,
                                                          string serverName,
                                                          int timeOutInterval,
                                                          X509Certificate2 clientCertyificate = null)
        {
            TcpClient client = null;

            if ( socket == null)
                throw new ArgumentNullException("socket");

            if (string.IsNullOrEmpty(serverName))
                throw new ArgumentNullException("serverName");

            
            client = new TcpClient(socket.AddressFamily);

            var sslService = new SslStreamService(client, serverName, timeOutInterval, clientCertyificate);
            AuthenticateAsClient(sslService,clientCertyificate,serverName);
            return sslService;
        }

 
        protected override void Dispose(bool disposing)
        {
            if (disposing && !base.IsDisposed)
            {
                base.Dispose(disposing);
                if (client != null)
                    client.Close();
            }
        }

        #region private methods
        private static void AuthenticateAsClient(SslStreamService sslService, X509Certificate2 clientCertyificate, string serverName)
        {
            if (clientCertyificate != null)
                sslService.Service.AuthenticateAsClient(serverName, new X509Certificate2Collection() { clientCertyificate }, SslProtocols.Default, false);
            else
                sslService.Service.AuthenticateAsClient(serverName);
        }

        private static bool ValidateServerCertificateCallback(object sender,
                                                     X509Certificate certificate,
                                                     X509Chain chain,
                                                     SslPolicyErrors sslPolicyErrors)
        {

            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            return false;
        }
        #endregion
    }



}
