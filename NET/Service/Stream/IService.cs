﻿namespace Service
{
    using System;

    public interface IService<TService> : IDisposable
    {
        bool IsDisposed { get; }
       
    }
}
