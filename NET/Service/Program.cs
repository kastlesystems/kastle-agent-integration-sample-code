﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                var serviceUrl = ConfigurationSettings.AppSettings["ServiceUrl"];
                Logger.SetLogWriter(new LogWriterFactory().Create());
                if (serviceUrl != null)
                {                    
                    SystemLogger.WriteError("Main: Application started!!");
                    Console.WriteLine(string.Format("Starting adapter service at {0}...", serviceUrl));
                    Console.WriteLine(string.Format("Service Admin Login: \"{0}\"", ConfigurationSettings.AppSettings["AdminName"]));
                    Console.WriteLine(string.Format("Service Password: \"{0}\"", ConfigurationSettings.AppSettings["AdminPassword"]));
                    ServiceManager.Start(serviceUrl);
                    Console.WriteLine("Adapter service started. Press any key to close.");
                    Console.ReadKey();
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }

            
        }
    }
}
