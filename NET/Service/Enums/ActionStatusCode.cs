﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public enum ActionStatusCode
    {
        //1-OK, 2-Device Busy, 3-Device Error, 4-Invalid Operation, 5-Invalid XML Format, 6-Invalid XML Content; 7-Reboot Required
        OK = 1,
        DeviceBusy = 2,
        DeviceError = 3,
        InvalidOperation = 4,
        InvalidXMLFormat = 5,
        InvalidXMLContent = 6,
        RebootRequired = 7
      

    }
}
