﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Service
{
    /// <summary>
    /// Generic EventArgs, to be used with Generic EventHandler for event handling model simplification. 
    /// </summary>
    /// <typeparam name="T">Type of event related object</typeparam>
    /// <example>
    /// Declaration
    /// <code>
    /// public event EventHandler<EventArgs<T>> SomethingHappened;
    /// </code>
    /// Usage (notice, that Fire method belongs to EventExtensions static class, that can be found in this namespace).
    /// <code>
    /// SomethingHappened.Fire(this, InstanceOfT);
    /// </code>
    /// </example>
    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T instance)
        {
            this.Instance = instance;
        }

        public T Instance { get; private set; }
    }

    /// <summary>
    /// This class consist of generic event handling extension methods.
    /// </summary>
    public static class EventExtensions
    {
        /// <summary>
        /// Usage example, can be found in EventArgs<T> documentation."/>
        /// </summary>
        /// <typeparam name="T">Type of event related object.</typeparam>
        /// <param name="handler">Generic event handler.</param>
        /// <param name="sender">Object that caused an event.</param>
        /// <param name="typeInstance">Instance of event related object.</param>
        public static void Fire<T>(this EventHandler<EventArgs<T>> handler, object sender, T typeInstance)
        {
            if (handler!=null) handler(sender, new EventArgs<T>(typeInstance));
        }
    }
}
