﻿using Newtonsoft.Json.Serialization;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.SelfHost;

namespace Service
{
    public static class ServiceManager
    {

        public static void Start(string url)
        {
            var config = new HttpSelfHostConfiguration(url);


            config.Formatters.XmlFormatter.UseXmlSerializer = true;
            config.Formatters.XmlFormatter.WriterSettings.OmitXmlDeclaration = false;
            config.Formatters.XmlFormatter.WriterSettings.Encoding = Encoding.UTF8;
            config.Formatters.XmlFormatter.RemoveSerializer(typeof(Stream));
            config.TransferMode = System.ServiceModel.TransferMode.StreamedResponse;
           // config.Services.Replace(typeof(ITraceWriter), new ErrorLogTraceWriter());

            config.SendTimeout = TimeSpan.MaxValue;
            config.MapHttpAttributeRoutes();



            var cors = new EnableCorsAttribute(
         origins: "*",
         headers: "*",
         methods: "*");
            config.EnableCors(cors);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", id = RouteParameter.Optional });

           
            var server = new HttpSelfHostServer(config);
            server.OpenAsync().Wait();
         
        }
    }
}
