﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace Service
{

   

    public static class StringExtensions
    {
        public static string Encapsulate(this string text, string with)
        {
            return String.Concat(with, text, with);
        }

        public static T FromXml<T>(this string xmlData) where T : class, new()
        {
            if (string.IsNullOrEmpty(xmlData)) return default(T);

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlData);
            }
            catch
            {
                return default(T);
            }

            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlData)))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return serializer.Deserialize(memStream) as T;
            }
        }



    }
}
