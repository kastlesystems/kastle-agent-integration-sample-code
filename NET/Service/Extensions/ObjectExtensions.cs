﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Service
{
    public class XmlTextWriterFull : XmlTextWriter
    {

        bool writeFullEndElement;

        public XmlTextWriterFull(TextWriter sink)
            : base(sink)
        {



        }

        public XmlTextWriterFull(Stream stream, Encoding enc) : base(stream, enc)
        {

        }
        public XmlTextWriterFull(String str, Encoding enc) : base(str, enc)
        {


        }

        public XmlTextWriterFull(Stream stream, Encoding enc, bool writeFullEndElement = true) : base(stream, enc)
        {

            this.writeFullEndElement = writeFullEndElement;
        }

        public override void WriteEndElement()
        {
            if (writeFullEndElement)
            {
                this.Formatting = System.Xml.Formatting.Indented;
                base.WriteFullEndElement();
            }
        }
    }


    public static class ObjectExtensions
    {
        public static T To<T>(string value)
        {
            return ((T)(Convert.ChangeType(value, typeof(T))));
        }


        public static string ToXml(this object objectToSerialize, bool writeFullEndElement = false, bool removeNamespaces = false, bool omitXmlDeclaration = false)
        {
            return ToXml(objectToSerialize, Encoding.UTF8, writeFullEndElement, removeNamespaces, omitXmlDeclaration);
        }

        public static string ToXml(this object objectToSerialize, Encoding encoding, bool writeFullEndElement = false, bool removeNamespaces = false, bool omitXmlDeclaration = false)
        {
            try
            {

                XmlSerializer serializer = new XmlSerializer(objectToSerialize.GetType(), string.Empty);
                if (serializer != null)
                {
                    using (var memStream = new MemoryStream())
                    {
                        if (writeFullEndElement)
                        {
                            var writer = new XmlTextWriterFull(memStream, encoding);
                            if (!removeNamespaces)
                            {
                                serializer.Serialize(writer, objectToSerialize);
                                return encoding.GetString(memStream.GetBuffer());
                            }
                            else
                            {
                                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                                ns.Add("", "");
                                serializer.Serialize(writer, objectToSerialize, ns);
                                return encoding.GetString(memStream.GetBuffer());
                            }

                        }
                        else
                        {
                            if (!removeNamespaces)
                            {


                                using (var memStm = new MemoryStream())
                                using (var xw = XmlWriter.Create(memStm, new XmlWriterSettings() { Encoding = encoding, OmitXmlDeclaration = omitXmlDeclaration }))
                                {
                                    serializer.Serialize(xw, objectToSerialize);
                                    return encoding.GetString(memStm.ToArray());
                                }


                            }
                            else
                            {
                                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                                ns.Add("", "");
                                using (var memStm = new MemoryStream())
                                using (var xw = XmlWriter.Create(memStm, new XmlWriterSettings() { Encoding = encoding, OmitXmlDeclaration = omitXmlDeclaration }))
                                {
                                    serializer.Serialize(xw, objectToSerialize, ns);
                                    return encoding.GetString(memStm.ToArray());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return string.Format("Unable to serialise object: <ErrorMessage>{0}</ErrorMessage>", ex.Message);
            }

            return null;
        }




        public static byte[] ObjectToByteArray(this object objectToSerialize)
        {
            var props = objectToSerialize.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance)
                                                   .Select(p => p.GetValue(objectToSerialize, null))
                                                   .ToArray();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, props);
                return memoryStream.ToArray();
            }
        }




        public static bool IsNull(this object o)
        {
            return o == null;
        }

        public static T Display<T>(this bool value, T ifTrue, T ifFalse)
        {
            return value ? ifTrue : ifFalse;
        }



    }
}
